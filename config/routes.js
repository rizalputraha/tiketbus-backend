/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: '404' },


  //  ╔═╗╔═╗╦
  //  ╠═╣╠═╝║
  //  ╩ ╩╩  ╩

  'POST /api/v1/user/login': 'auth.LoginController.login',
  'POST /api/v1/member/login': 'auth.LoginController.loginMember',
  'POST /api/v1/user/refresh_token': 'auth.LoginController.refreshToken',
  'POST /api/v1/member/refresh_token': 'auth.LoginController.refreshTokenMember',
  '/api/v1/user/logout': 'auth.LoginController.logout',
  'POST /api/v1/user/signup': 'auth.SignupController.register',

  // USER
  'POST /api/v1/user/create': 'UserController.create',
  'GET /api/v1/user/list': 'UserController.listUser',
  'GET /api/v1/user/detail': 'UserController.detailUser',
  'POST /api/v1/user/update/:id': 'UserController.update',
  'POST /api/v1/user/delete/:id': 'UserController.delete',

  // MEMBER
  'POST /api/v1/member/create': 'MemberController.create',
  'GET /api/v1/member/list': 'MemberController.listMember',
  'GET /api/v1/member/detail': 'MemberController.detailMember',
  'POST /api/v1/member/update/:id': 'MemberController.update',
  'POST /api/v1/member/delete/:uid': 'MemberController.delete',

  // BUS
  'POST /api/v1/bus/create': 'BusController.create',
  'GET /api/v1/bus/read': 'BusController.listBus',
  'GET /api/v1/bus/detail/:id': 'BusController.detail',
  'POST /api/v1/bus/update/:id': 'BusController.update',
  'POST /api/v1/bus/delete/:id': 'BusController.delete',

  // PO BUS
  'POST /api/v1/pobus/create': 'PobusController.create',
  'GET /api/v1/pobus/read': 'PobusController.read',
  'GET /api/v1/pobus/list-employe': 'PobusController.listEmployer',
  'POST /api/v1/pobus/update/:id': 'PobusController.update',
  'POST /api/v1/pobus/delete/:id': 'PobusController.delete',

  // TRAYEK
  'POST /api/v1/trayek/create': 'TrayekController.create',
  'GET /api/v1/trayek/read': 'TrayekController.read',
  'GET /api/v1/trayek/read/no_auth': 'TrayekController.listTrayekNoAuth',
  'POST /api/v1/trayek/update/:id': 'TrayekController.update',
  'POST /api/v1/trayek/delete/:id': 'TrayekController.delete',
  'POST /api/v1/trayek/kota/delete/:id': 'TrayekController.deleteKotaSinggah',
  'GET /api/v1/caritrayek': 'TrayekController.cekTrayek',
  'GET /api/v1/caritrayekpp': 'TrayekController.pulangPergi',

  // JAM TRAYEK
  'POST /api/v1/jam_trayek/create': 'JamtrayekController.create',
  'GET /api/v1/jam_trayek/read': 'JamtrayekController.read',
  'GET /api/v2/jam_trayek/read': 'JamtrayekController.readV2',
  'GET /api/v1/jam_trayek/read/:id': 'JamtrayekController.readById',
  'POST /api/v1/jam_trayek/update/:id': 'JamtrayekController.update',
  'POST /api/v1/jam_trayek/delete/:id': 'JamtrayekController.delete',

  // AGEN
  'POST /api/v1/agen/create': 'AgenController.create',
  'GET /api/v1/agen/read': 'AgenController.read',
  'POST /api/v1/agen/update/:id': 'AgenController.update',
  'POST /api/v1/agen/delete/:id': 'AgenController.delete',

  // CREW
  'POST /api/v1/crew/create': 'CrewController.create',
  'GET /api/v1/crew/read': 'CrewController.read',
  'GET /api/v1/crew/detail': 'CrewController.detail',
  'POST /api/v1/crew/update/:id': 'CrewController.update',
  'POST /api/v1/crew/delete/:id': 'CrewController.delete',

  // CHECKIN BUS
  'POST /api/v1/checkinbus/create': 'CheckinbusController.create',
  'POST /api/v1/checkinbus/update/:id': 'CheckinbusController.update',
  'GET /api/v1/checkinbus/read': 'CheckinbusController.read',
  'GET /api/v1/checkinbus/check': 'CheckinbusController.check',

  // KOTA
  'GET /api/v1/kota': 'KotaController.kota',
  'POST /api/v1/kota/create': 'KotaController.create',
  'POST /api/v1/kota/update/:id': 'KotaController.update',
  'POST /api/v1/kota/delete/:id': 'KotaController.delete',

  // KURSI
  'POST /api/v1/kursi/create': 'KursiController.create',
  'GET /api/v1/kursi/bus_kursi': 'KursiController.showKursi',
  'GET /api/v1/kursi/read': 'KursiController.read',
  'POST /api/v1/kursi/update/:id': 'KursiController.update',
  'POST /api/v1/kursi/updatecrew': 'KursiController.updatecrew',
  'POST /api/v1/kursi/delete/:id': 'KursiController.delete',

  // TRANSCREW
  'POST /api/v1/transcrew/create': 'TranscrewController.create',
  'GET /api/v1/transcrew/read': 'TranscrewController.read',
  'GET /api/v1/historipo/crew': 'TranscrewController.historyPo',
  'GET /api/v1/crew/kursi/detail/:kdKursi': 'TranscrewController.detailKursi',

  // TRANSAGEN
  'POST /api/v1/transagen/create': 'TransagenController.create',
  'GET /api/v1/transagen/read': 'TransagenController.read',
  'GET /api/v1/transagen/detail': 'TransagenController.detail',
  'GET /api/v1/historipo/agen': 'TransagenController.historyPo',
  'GET /api/v1/transagen/calon_penumpang': 'TransagenController.calonPenumpang',
  'POST /api/v1/transagen/update/:id': 'TransagenController.update',
  'POST /api/v1/transagen/lunas/:id': 'TransagenController.updateLunas',
  'GET /api/v1/transagen/konfirmasi_kursi/:kode': 'TransagenController.konfirmasiKursi',
  'POST /api/v1/generate/pdf': 'LibraryController.pdf',

  // TRANSCUST
  // 'POST /api/v1/transcust/create': 'TranscustController.create',
  // 'POST /api/v1/transcust/transaction': 'TranscustController.transactionsBri',
  'POST /api/v1/transcust/transaction': 'TranscustController.transactionsBriV2',
  'POST /api/v1/transcust/cek_transaction': 'TranscustController.checkPayments',
  'GET /api/v1/transcust/list_transaction': 'TranscustController.listTransactionDB',
  'GET /api/v1/create_cron': 'TranscustController.tryTestCron',

  // BANK
  'GET /api/v1/bank/read': 'BankController.read',

  // TIKET
  'GET /api/v1/tiket/cek_tiket': 'TiketController.checkTiketV2',
  'GET /api/v1/tiket/cek_tiket/no_auth': 'TiketController.checkTiketNoAuth',
  // 'GET /api/v1/tiket/cek_tiket_v2': 'TiketController.checkTiketV2',

  // CHECK
  // BRI
  'POST /api/v1/check/modulbri': 'TranscustController.credentialsBankBri', //routes ini tidak bisa diubah, kayak eeq
  'POST /api/v1/check/create/briva': 'TranscustController.createBriVa',
  'GET /api/v1/check/get/briva/:institution_code/:briva_no/:customer_code': 'TranscustController.checkVaBri',
  'GET /api/v1/check/get/briva/status/:institution_code/:briva_no/:customer_code': 'TranscustController.checkVaBriStatus',
  'PUT /api/v1/check/update_status/briva': 'TranscustController.updateStatusBriva',
  'PUT /api/v1/check/update/briva': 'TranscustController.updateBriva',
  'POST /api/v1/check/update/briva': 'TranscustController.updateBriva',
  'GET /api/v1/check/get/report/briva/:institutionCode/:brivaNo/:startDate/:endDate': 'TranscustController.getReportBri',
  'DELETE /api/v1/check/get/delete/briva/:kode_va': 'TranscustController.deleteBriva',

  // BNI
  'POST /api/v1/check/bni/billing/create': 'TranscustController.createBillingBni',
  'POST /api/v1/check/bni/notif': 'TranscustController.sendNotifBniPost',
  'GET /api/v1/check/bni/notif': 'TranscustController.sendNotifBniGet',
  'POST /api/v1/check/bni/inquiry': 'TranscustController.inquiryBilling',
  'POST /api/v1/check/bni/billing/update': 'TranscustController.updateTransaction',

  // BCA
  // 'POST /api/v1/check/bca_credentials': 'TranscustController.creadentialsBankBca', //INI BUAT INQUIRY
  'POST /api/oauth/token': 'TranscustController.creadentialsBankBca', //INI BUAT INQUIRY
  'POST /api/v1/check/saldo/bca_credentials': 'TranscustController.credentialsSaldoBca', //INI BUAT CHECK SALDO/MUTASI
  // 'POST /api/v1/check/va/bills': 'TranscustController.vaBillsBca',
  // 'POST /api/v1/check/va/payments': 'TranscustController.vaPaymentsBca',
  'GET /api/v1/check/inquiry/:company_code/:customer_code': 'TranscustController.inquiryPaymentBca',
  'GET /api/v1/check/inquiry_reqid/:company_code/:request_id': 'TranscustController.inquiryPaymentReqIdBca',
  'GET /api/v1/check/banking_saldo/:company_code/:customer_code': 'TranscustController.bankingSaldoBca',
  'GET /api/v1/check/mutasi/:company_code/:customer_code': 'TranscustController.checkMutasiBca',
  'POST /api/v1/check/online_transfer': 'TranscustController.transferOnlineBca',

  // Testing Send Email
  'GET /api/v1/send_mail': 'LibraryController.sendEmail',

  // PROTRACK
  'GET /api/v1/protrack/auth':'ProtrackController.read',
  'GET /api/v1/protrack/update':'ProtrackController.trace',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
