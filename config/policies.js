/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,
  '*': 'isAuthenticated',
  'auth/*':true,
  MemberController:{
    '*': 'isAuthenticated',
    'create' : true,
  },
  TrayekController : {
    '*': 'isAuthenticated',
    'cekTrayek' : true,
    'pulangPergi' : true,
    'listTrayekNoAuth' : true
  },
  KotaController : {
    'kota':true
  },
  TiketController : {
    'checkTiketNoAuth':true
  },
  TranscustController: {
    'sendNotifBniPost':true,
    'sendNotifBniGet':true
  },
  ProtrackController: {
   '*': true
  }
};
