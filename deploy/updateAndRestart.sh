#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/islam/backend

git pull git@gitlab.com:b36/tiketbus-backend.git master

# install npm packages
npm install

# Restart the node server
source ~/.profile

pm2 restart backend