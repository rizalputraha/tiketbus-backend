/**
 * TranscustController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const BrimodulController = require('../controllers/bank/BrimodulController');
const BnimodulController = require('../controllers/bank/BnimodulController');
const env = require('../../env');
// const moment = require('moment');
let moment = require('moment-timezone');
var cron = require('node-cron');
const BcamodulController = require('./bank/BcamodulController');
moment().tz('Indonesia/Jakarta').format();

const TranscustController = {
    createProcess: async function (body, arrTicket) {
        body.created_at = moment().format('YYYY-MM-DD HH:mm:ss').valueOf();
        body.updated_at = moment().format('YYYY-MM-DD HH:mm:ss').valueOf();
        let resp = await Transcust.create(body);
        return true;
    },

    historyTransaksi: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Transcust.find({}).populate('id_member').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate('id_bus').populate('id_paymentcust');
            return resp;
        })
    },

    listTransactionDB: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {id_member} = res.locals.udata.payload;
            const result = await Transcust.find({where: {id_member: id_member}}).populate('id_pobus').populate('id_jamtrayek').populate('id_bus').populate('id_memberva').meta({skipRecordVerification: true});
            for (let i = 0; i < result.length; i++) {
                const el = result[i];
                const jt = await Jamtrayek.findOne({where: el.id_jamtrayek.id}).populate('id_trayek');
                const k_asal = await Kota.findOne({where: jt.id_trayek.id_kota_asal});
                const k_tujuan = await Kota.findOne({where: jt.id_trayek.id_kota_tujuan});
                const member = await Member.findOne({where: id_member});
                el.id_jamtrayek = {
                    ...jt,
                    id_trayek : {
                        ...jt.id_trayek,
                        id_kota_asal: k_asal,
                        id_kota_tujuan: k_tujuan
                    }
                };
                el.id_member = member;
            }
            return result;
        })
    },

    // BCA
    // creadentialsBankBca: async (req, res) => {
    //     await LibraryController.response(req, res, async (body) => {
    //         const bca = await BcamodulController.BcaCredentials(req.body);
    //         return "Success Create Access Token";
    //     });
    // }, with npc-token

    creadentialsBankBca: async (req, res) => {
        const bca = await BcamodulController.BcaCredentials(req.body);
        return await res.json({
            "access_token":"2YotnFZFEjr1zCsicMWpAA",
            "token_type":"Bearer",
            "expires_in":3600,
            "scope":"resource.WRITE resource.READ"
        });
    },

    credentialsSaldoBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const bca = await BcamodulController.BcaCredentials2(req.body);
            return "Success Create Access Token";
        });
    },

    // vaBillsBca: async (req, res) => {
    //     await LibraryController.response(req, res, async (body) => {
    //         const bca = await BcamodulController.bcaBills(req.body);
    //         return bca;
    //     });
    // },

    // vaPaymentsBca: async (req, res) => {
    //     await LibraryController.response(req, res, async (body) => {
    //         const bca = await BcamodulController.bcaPayments(req.body);
    //         return bca;
    //     });
    // },

    inquiryPaymentBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {company_code, customer_code} = req.params;
            const bca = await BcamodulController.inquiryPayment(company_code, customer_code);
            return bca;
        });
    },

    inquiryPaymentReqIdBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {company_code, request_id} = req.params;
            const bca = await BcamodulController.inquiryPaymentReqId(company_code, request_id);
            return bca;
        });
    },

    bankingSaldoBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {company_code, customer_code} = req.params;
            const bca = await BcamodulController.bankingSaldo(company_code, customer_code);
            return bca;
        });
    },

    checkMutasiBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {company_code, customer_code} = req.params;
            const {startDate, endDate} = req.query;
            const bca = await BcamodulController.mutasiBanking(company_code, customer_code, startDate, endDate);
            return bca;
        });
    },

    transferOnlineBca: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const bca = await BcamodulController.onlineTransfer(req.body);
            return bca;
        });
    },

    // END BCA

    // BRI
    credentialsBankBri: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {client_id, client_secret} = req.body;
            const {grant_type} = req.query;
            const bri = await BrimodulController.briClientCredential(client_id, client_secret, grant_type); // CREDENTIAL BERHASIL
            return bri;
        })
    },

    createBriVa: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(15, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            const { amount, id_bank } = req.body;
            const { id_member, name, telpon } = res.locals.udata.payload;
            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank});
            let createVatoBri;

            if (typeof amount == "string" || amount < 0 ) {
                throw new Error('Amount must be number format');
            }
            
            if (!checkVa) {
                const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
                const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
                const auth = `Bearer ${bri.access_token}`;
                const bodyBri = {
                    "institutionCode": env.institution_code_bri,
                    "brivaNo": env.briva_no_bri,
                    "custCode": `${telpon}`,
                    "nama": name,
                    "amount": amount,
                    "keterangan": "Create Transaction",
                    "expiredDate": dueTime
                }
                createVatoBri = await BrimodulController.briCreateVirtualAccount(bodyBri, auth);
                if (createVatoBri.status == true) {
                    const data = {
                        id_member,
                        id_bank,
                        status:0,
                        kd_va:createVatoBri.data.custCode,
                        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                        updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                    createMemberVa = await Memberva.create(data).fetch();
                    return createVatoBri;
                }
                return createVatoBri;
            } else {
                throw new Error('Member sudah terdaftar')
            }
        })
    },

    checkVaBri: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
            const auth = `Bearer ${bri.access_token}`;
            const resGetVa = await BrimodulController.getVirtualAccountInfo(req.params, auth);
            return resGetVa;
        })
    },

    checkVaBriStatus: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
            const auth = `Bearer ${bri.access_token}`;
            const resGetVa = await BrimodulController.getVirtualAccountStatus(req.params, auth);
            return resGetVa;
        })
    },

    getReportBri: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
            const auth = `Bearer ${bri.access_token}`;
            const resGetReport = await BrimodulController.getReportVa(req.params, auth);
            return resGetReport;
        })
    },

    updateStatusBriva: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            let data = req.body;
            const { id_bank, custCode } = req.body;
            const { id_member } = res.locals.udata.payload;
            const auth = `Bearer ${bri.access_token}`;

            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank});
            if (checkVa) {
                data.institutionCode = env.institution_code_bri;
                data.brivaNo = env.briva_no_bri;
                data.custCode = custCode;
                const resGetReport = await BrimodulController.updStatusPayVa(data, auth);
                return resGetReport;
            } else {
                throw new Error("Anda belum mempunyai nomor va");
            }
        })
    },

    updateBriva: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(15, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            const data = req.body;
            const { id_bank } = req.body;
            const { id_member } = res.locals.udata.payload;
            const auth = `Bearer ${bri.access_token}`;

            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank});
            if (checkVa) {
                data.institutionCode = env.institution_code_bri;
                data.brivaNo = env.briva_no_bri;
                data.custCode = checkVa.kd_va;
                data.expiredDate = dueTime;
                const resGetReport = await BrimodulController.updateVa(data, auth);
                return resGetReport;
            } else {
                throw new Error("Anda belum mempunyai nomor va");
            }
        })
    },

    deleteBriva: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {kode_va} = req.params;
            if (kode_va == 0) {
                throw new Error("Customer Code Tidak Boleh Kosong");
            }
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            const { data, id_bank } = req.body;
            // if (data.indexOf('custCode=0') == 42) {
            //     throw new Error("Customer Code Tidak Boleh Kosong");
            // } 
            if (data.indexOf('institutionCode=0') == 0) {
                throw new Error("Institution Code Tidak Boleh Kosong");
            }
            // const strng = JSON.stringify(data);
            // const { id_member } = res.locals.udata.payload;
            const auth = `Bearer ${bri.access_token}`;

            const checkVa = await Memberva.findOne({kd_va: kode_va, id_bank: id_bank});
            if (checkVa) {
                const deleteVaBri = await BrimodulController.deleteVa(data, auth);
                if (deleteVaBri.status == true) {
                    // const deleteVa = await Memberva.updateOne({kd_va: kode_va}).set({status: 1}).meta({skipRecordVerification: true});
                    const deleteVa = await Memberva.destroyOne({id: checkVa.id});
                    return deleteVaBri;   
                } else {
                    return deleteVaBri;
                }
            } else {
                throw new Error("Data Customer Tidak Ditemukan");
            }
        });
    },
    
    checkPayments: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {telpon} = res.locals.udata.payload
            const findData = await Memberva.findOne({kd_va: telpon});
            const {id_tiket} = req.body

            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            const auth = `Bearer ${bri.access_token}`;
            const checkStatus = {
                institution_code: env.institution_code_bri,
                briva_no: env.briva_no_bri,
                customer_code: telpon
            }
            const resGetVa = await BrimodulController.getVirtualAccountInfo(checkStatus, auth);
            // console.log({resGetVa});
            if (!findData) {
                throw new Error("Data not found!");
            }
            if (resGetVa.status == true) {
                if (resGetVa.data.statusBayar == "Y") {
                    const objDelete = {
                        data : `institutionCode=CGRCUY940Y5&brivaNo=12816&custCode=${telpon}`,
                        id_bank : findData.id_bank
                    }
                    const deleteVaInBri = await BrimodulController.deleteVa(objDelete.data, auth);
                    const upd = await Transcust.updateOne({id: id_tiket}).set({status: 1, updated_at: moment().format('YYYY-MM-DD HH:mm:ss')});
                    if (deleteVaInBri.status == true) {
                        const deleteVaDB = await Memberva.updateOne({id: findData.id}).set({status: 0}); // status 0 = kembali normal (no action)
                        return "Tiket berhasil dibayar";
                    } else {
                        throw new Error("Gagal Hapus VA");
                    }
                } else {
                    // return "Belum melakukan pembayaran";
                    throw new Error("Belum melakukan pembayaran");
                }
            } else {
                return resGetVa;
            }
        })
    },

    // delete briva when successfully pay
    deleteBrivaAuto2: async (id_tiket, telpon) => {
        let auth;
        const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
        const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
        auth = `Bearer ${bri.access_token}`;
        cron.schedule('* * * * * *', async () =>  {
            console.log('Cron Actived For Check Payment BRI When Got Status Bayar Y');
            const checkDb = await Memberva.findOne({where: {kd_va: telpon, status: 1}});
            if (checkDb) {
                const checkStatus = {
                    institution_code: env.institution_code_bri,
                    briva_no: env.briva_no_bri,
                    customer_code: telpon
                }
                const resGetVa = await BrimodulController.getVirtualAccountInfo(checkStatus, auth);
                // console.log({resGetVa});
                if (resGetVa.status == false || resGetVa.responseCode == '14') {
                    console.log("briva not found");
                    return 'Data Virtual Account Tidak ditemukan';
                } else if(resGetVa.status == true) {
                    if (resGetVa.data.statusBayar == 'Y') {
                        const findData = await Memberva.findOne({kd_va: telpon});
                        const objDelete = {
                            data : `institutionCode=CGRCUY940Y5&brivaNo=12816&custCode=${telpon}`
                        }
                        const deleteVaInBri = await BrimodulController.deleteVa(objDelete.data, auth);
                        const upd = await Transcust.updateOne({id: id_tiket}).set({status: 1, updated_at: moment().format('YYYY-MM-DD HH:mm:ss')});
                        const deleteVaDB = await Memberva.updateOne({id: findData.id}).set({status: 0});
                        console.log("briva found & delete");
                        return 'Data Berhasil Dihapus';
                    }
                }
            } else {
                console.log("tidak ada transaksi");
            }
            // return;
        });
        // task.stop();
    },
    // end delete briva when successfully pay

    // delete briva when don't pay
    deleteBrivaAuto : async (dueTime, id_tiket, telpon) => {
        // console.log(dueTime);
        const subHours = dueTime.substring(11, 13);
        const subMin = dueTime.substring(14, 16);
        cron.schedule(`${subMin} ${subHours} * * *`, async () => {
            console.log('Cron Active');
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            const auth = `Bearer ${bri.access_token}`;
            const checkStatus = {
                institution_code: env.institution_code_bri,
                briva_no: env.briva_no_bri,
                customer_code: telpon
            }
            const resGetVa = await BrimodulController.getVirtualAccountInfo(checkStatus, auth);
            // console.log({resGetVa});
            if (resGetVa.status == false || resGetVa.responseCode == '14') {
                console.log("briva not found");
                return 'Data Virtual Account Tidak ditemukan';
            } else if(resGetVa.status == true) {
                if (resGetVa.data.statusBayar == 'N') {
                    const findData = await Memberva.findOne({kd_va: telpon});
                    const objDelete = {
                        data : `institutionCode=CGRCUY940Y5&brivaNo=12816&custCode=${telpon}`
                    }
                    const deleteVaInBri = await BrimodulController.deleteVa(objDelete.data, auth);
                    const upd = await Transcust.updateOne({id: id_tiket}).set({status: 2, updated_at: moment().format('YYYY-MM-DD HH:mm:ss')});
                    const deleteVaDB = await Memberva.updateOne({id: findData.id}).set({status: 0});
                    console.log("briva found & delete");
                    return 'Data Berhasil Dihapus';
                }
            }
        });
        // cronCheck.stop();
        // cronCheck.destroy();
    },
    // end delete briva when don't pay

    transactionsBriV2: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(5, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            // const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(2, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            // console.log("check duetime", dueTime);
            let createMemberVa;
            let arrTicket = [];
            let auth;
            const { id_pobus, id_member, id_jamtrayek, id_bus, total, status, qty, penumpang, id_bank } = req.body;
            const {name,telpon} = res.locals.udata.payload
            if (typeof total == "string" || total < 0 ) {
                throw new Error('Amount must be number format');
            }
            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank});
            // console.log({checkVa});
            const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
            const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
            
            auth = `Bearer ${bri.access_token}`;
            if (checkVa) {
                const bodyBri = {
                    "institutionCode": env.institution_code_bri,
                    "brivaNo": env.briva_no_bri,
                    "custCode": `${telpon}`,
                    "nama": name,
                    "amount": total,
                    "keterangan": "Create Transaction",
                    "expiredDate": dueTime
                }
                createVatoBri = await BrimodulController.briCreateVirtualAccount(bodyBri, auth);
                // await TranscustController.deleteBrivaAuto(dueTime);
                if (createVatoBri.status == true) {
                    createMemberVa = await Memberva.update({id_member: id_member}).set({status: 1}).fetch(); //status 1 = sudah order tapi belum melakukan pembayaran
                    console.log({createMemberVa});
                }
            } else {
                // const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
                // const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
                // const auth = `Bearer ${bri.access_token}`;
                const bodyBri = {
                    "institutionCode": env.institution_code_bri, // sandbox
                    "brivaNo": env.briva_no_bri, // sandbox
                    "custCode": `${telpon}`,
                    "nama": name,
                    "amount": total,
                    "keterangan": "Create Transaction",
                    "expiredDate": dueTime
                }
                createVatoBri = await BrimodulController.briCreateVirtualAccount(bodyBri, auth);
                // await TranscustController.deleteBrivaAuto(dueTime);
                if (createVatoBri.responseCode == '00' || createVatoBri.responseCode == '13') {
                    const data = {
                        id_member,
                        id_bank,
                        status:1,
                        kd_va:createVatoBri.data.custCode,
                        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                        updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                    createMemberVa = await Memberva.create(data).fetch();
                    // return createVatoBri;
                }
            }
            const valMemberVa = checkVa ? checkVa.id : createMemberVa.id
            const dataTrans = {
                kd_transaksi: 'MBR' + moment().format('DDMMYY') + id_member + LibraryController.randomNumber(),
                id_member,
                id_memberva: valMemberVa,
                id_pobus,
                id_jamtrayek,
                id_bus,
                qty,
                total,
                due_date:dueTime,
                status,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
            };
            const createTrans = await Transcust.create(dataTrans).fetch().meta({skipRecordVerification: true});
            await TranscustController.deleteBrivaAuto(dueTime, createTrans.id, telpon);
            await TranscustController.deleteBrivaAuto2(createTrans.id, telpon);
            if (createTrans) {
                const member = await Member.findOne({where: createTrans.id_member});
                const memberva = await Memberva.findOne({where: createTrans.id_memberva});
                const jt = await Jamtrayek.findOne({where: createTrans.id_jamtrayek}).populate('id_trayek');
                const k_asal = await Kota.findOne({where: jt.id_trayek.id_kota_asal});
                const k_tujuan = await Kota.findOne({where: jt.id_trayek.id_kota_tujuan});
                const data = {
                    ...createTrans,
                    id_member: member,
                    id_memberva: memberva,
                    id_jamtrayek: {
                        ...jt,
                        id_trayek:{
                            ...jt.id_trayek,
                            id_kota_asal: k_asal,
                            id_kota_tujuan: k_tujuan,
                        }
                    } 
                }
                LibraryController.sendEmail(data);
            }
            for (let iT = 0; iT < qty; iT++) {
                let tanggal_smp;
                const jt = await Jamtrayek.findOne({id:id_jamtrayek}).meta({skipRecordVerification:true});
                const start = moment(jt.jam_berangkat, "HH:mm");
                const end = moment(jt.jam_sampai, "HH:mm");
                if(end.diff(start) < 0 ) {
                    tanggal_smp = moment().add(1, 'd').format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                } else {
                    tanggal_smp = moment().format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                }

                const dataTicket = {
                    id_kursi:penumpang[iT].id_kursi, 
                    id_transcust: createTrans.id, 
                    nama:penumpang[iT].nama, 
                    no_telp:penumpang[iT].no_telp, 
                    harga:penumpang[iT].harga, 
                    status:penumpang[iT].status, 
                    tgl_beli:penumpang[iT].tgl_beli, 
                    tgl_brgkt:penumpang[iT].tgl_brgkt,
                    tgl_smp:penumpang[iT].tanggal_smp,
                    created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                };
                arrTicket.push(dataTicket);
            };
            const createTickets = await Tiket.createEach(arrTicket).fetch().meta({skipRecordVerification: true});
            for (let iK = 0; iK < qty; iK++) {
                await Kursi.updateOne({ id: createTickets[iK].id_kursi }).set({ available: 1 }).meta({skipRecordVerification: true});
            };
            const objReturn = {
                no_va : !checkVa ? env.briva_no_bri+createMemberVa.kd_va : env.briva_no_bri+checkVa.kd_va,
                id_transcust : createTrans.id
            }
            // return {createTrans, createTickets};
            return objReturn;
        })
    },

    transactionsBri: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(15, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            let createMemberVa;
            let arrTicket = [];
            const { id_pobus, id_member, id_jamtrayek, id_bus, total, status, qty, penumpang, id_bank } = req.body;
            const {name,telpon} = res.locals.udata.payload
            if (typeof total == "string" || total < 0 ) {
                throw new Error('Amount must be number format');
            }
            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank, status: 0});
            if (!checkVa) {
                const briAuth = {client_id: env.client_id_bri, client_secret: env.client_secret_bri, grant_type: 'client_credentials'}
                const bri = await BrimodulController.briClientCredential(briAuth.client_id, briAuth.client_secret, briAuth.grant_type);
                
                const auth = `Bearer ${bri.access_token}`;
                const bodyBri = {
                    "institutionCode": env.institution_code_bri, // sandbox
                    "brivaNo": env.briva_no_bri, // sandbox
                    "custCode": `${telpon}`,
                    "nama": name,
                    "amount": total,
                    "keterangan": "Create Transaction",
                    "expiredDate": dueTime
                }
                createVatoBri = await BrimodulController.briCreateVirtualAccount(bodyBri, auth);
                if (createVatoBri.status == true) {
                    const data = {
                        id_member,
                        id_bank,
                        status:0,
                        kd_va:createVatoBri.data.custCode,
                        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                        updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                    createMemberVa = await Memberva.create(data).fetch();
                    // return createVatoBri;
                }
                // return createVatoBri;
            }
            const dataTrans = {
                kd_transaksi: 'MBR' + moment().format('DDMMYY') + id_member + LibraryController.randomNumber(),
                id_member,
                id_memberva: !checkVa ? createMemberVa.id :checkVa.id,
                id_pobus,
                id_jamtrayek,
                id_bus,
                qty,
                total,
                due_date:dueTime,
                status,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
            };
            const createTrans = await Transcust.create(dataTrans).fetch().meta({skipRecordVerification: true});
            for (let iT = 0; iT < qty; iT++) {
                let tanggal_smp;
                const jt = await Jamtrayek.findOne({id:id_jamtrayek}).meta({skipRecordVerification:true});
                const start = moment(jt.jam_berangkat, "HH:mm");
                const end = moment(jt.jam_sampai, "HH:mm");
                if(end.diff(start) < 0 ) {
                    tanggal_smp = moment().add(1, 'd').format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                } else {
                    tanggal_smp = moment().format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                }

                const dataTicket = {
                    id_kursi:penumpang[iT].id_kursi, 
                    id_transcust: createTrans.id, 
                    nama:penumpang[iT].nama, 
                    no_telp:penumpang[iT].no_telp, 
                    harga:penumpang[iT].harga, 
                    status:penumpang[iT].status, 
                    tgl_beli:penumpang[iT].tgl_beli, 
                    tgl_brgkt:penumpang[iT].tgl_brgkt,
                    tgl_smp:penumpang[iT].tanggal_smp,
                    created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                };
                arrTicket.push(dataTicket);
            };
            const createTickets = await Tiket.createEach(arrTicket).fetch().meta({skipRecordVerification: true});
            for (let iK = 0; iK < qty; iK++) {
                await Kursi.updateOne({ id: createTickets[iK].id_kursi }).set({ available: 1 }).meta({skipRecordVerification: true});
            };
            // return {createTrans, createTickets};
            return !checkVa ? env.briva_no_bri+createMemberVa.kd_va : env.briva_no_bri+checkVa.kd_va;
        })
    },
    // END BRI

    // BNI
    createBillingBni: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const dueTime = moment(moment(), 'YYYY-MM-DD HH:mm:ss').add(5, 'minutes').format('YYYY-MM-DD HH:mm:ss');
            let createMemberVa;
            let arrTicket = [];
            let kd_va;
            
            const { id_pobus, id_member, id_jamtrayek, id_bus, total, status, qty, penumpang, id_bank } = req.body;
            const {name,phone,email} = res.locals.udata.payload
            const kd_transaksi = 'MBR' + moment().format('DDMMYY') + id_member + LibraryController.randomNumber()
            if (typeof total == "string" || total < 0 ) {
                throw new Error('Amount must be number format');
            }

            const checkVa = await Memberva.findOne({id_member: id_member, id_bank:id_bank});
            
            if(checkVa) {
                // Create VA
                let unq = "";
                for(let i=0; i<(8-id_member.length); i++) {
                    const r = Math.floor(Math.random() * 9)
                    unq = unq + r.toString()
                }
                unq = unq+id_member.toString();
                kd_va = "98881115" + unq;
                const data = {
                    type: "createBilling",
                    trx_id: Date.now(),
                    trx_amount: total,
                    customer_name: name,
                    customer_email: email,
                    customer_phone: phone,
                    virtual_account: kd_va,
                    description: "Payment for " + kd_transaksi
                }

                const result = await BnimodulController.createInvoiceBilling(data);
                // END Create VA
                if (result.status == "000") {
                    createMemberVa = await Memberva.update({id_member: id_member}).set({status: 1}).fetch(); //status 1 = sudah order tapi belum melakukan pembayaran
                }
            } else {
                // Create VA
                let unq = "";
                for(let i=0; i<(8-id_member.length); i++) {
                    const r = Math.floor(Math.random() * 9)
                    unq = unq + r.toString()
                }
                unq = unq+id_member.toString();
                kd_va = "98881115" + unq;
                const data = {
                    type: "createBilling",
                    trx_id: Date.now(),
                    trx_amount: total,
                    customer_name: name,
                    customer_email: email,
                    customer_phone: phone,
                    virtual_account: kd_va,
                    description: "Payment for " + kd_transaksi
                }

                const result = await BnimodulController.createInvoiceBilling(data);
                // END Create VA
                if (result.status == "000") {
                    const data = {
                        id_member,
                        id_bank,
                        status:1,
                        kd_va:kd_va,
                        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                        updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                    createMemberVa = await Memberva.create(data).fetch();   
                }
            }

            const valMemberVa = checkVa ? checkVa.id : createMemberVa.id
            const dataTrans = {
                kd_transaksi: kd_transaksi,
                id_member,
                id_memberva: valMemberVa,
                id_pobus,
                id_jamtrayek,
                id_bus,
                qty,
                total,
                due_date:dueTime,
                status,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
            };
            const createTrans = await Transcust.create(dataTrans).fetch().meta({skipRecordVerification: true});
            if (createTrans) {
                const member = await Member.findOne({where: createTrans.id_member});
                const memberva = await Memberva.findOne({where: createTrans.id_memberva});
                const jt = await Jamtrayek.findOne({where: createTrans.id_jamtrayek}).populate('id_trayek');
                const k_asal = await Kota.findOne({where: jt.id_trayek.id_kota_asal});
                const k_tujuan = await Kota.findOne({where: jt.id_trayek.id_kota_tujuan});
                const data = {
                    ...createTrans,
                    id_member: member,
                    id_memberva: memberva,
                    id_jamtrayek: {
                        ...jt,
                        id_trayek:{
                            ...jt.id_trayek,
                            id_kota_asal: k_asal,
                            id_kota_tujuan: k_tujuan,
                        }
                    } 
                }
                LibraryController.sendEmail(data);
            }
            for (let iT = 0; iT < qty; iT++) {
                let tanggal_smp;
                const jt = await Jamtrayek.findOne({id:id_jamtrayek}).meta({skipRecordVerification:true});
                const start = moment(jt.jam_berangkat, "HH:mm");
                const end = moment(jt.jam_sampai, "HH:mm");
                if(end.diff(start) < 0 ) {
                    tanggal_smp = moment().add(1, 'd').format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                } else {
                    tanggal_smp = moment().format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
                }

                const dataTicket = {
                    id_kursi:penumpang[iT].id_kursi, 
                    id_transcust: createTrans.id, 
                    nama:penumpang[iT].nama, 
                    no_telp:penumpang[iT].no_telp, 
                    harga:penumpang[iT].harga, 
                    status:penumpang[iT].status, 
                    tgl_beli:penumpang[iT].tgl_beli, 
                    tgl_brgkt:penumpang[iT].tgl_brgkt,
                    tgl_smp:penumpang[iT].tanggal_smp,
                    created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                };
                arrTicket.push(dataTicket);
            };
            const createTickets = await Tiket.createEach(arrTicket).fetch().meta({skipRecordVerification: true});
            for (let iK = 0; iK < qty; iK++) {
                await Kursi.updateOne({ id: createTickets[iK].id_kursi }).set({ available: 1 }).meta({skipRecordVerification: true});
            };
            const objReturn = {
                no_va : !checkVa ? createMemberVa.kd_va : checkVa.kd_va,
                id_transcust : createTrans.id
            }
            // return {createTrans, createTickets};
            return objReturn;
        })
    },

    inquiryBilling: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const result = await BnimodulController.checkInquiryBilling(req.body);
            return result;
        });
    },

    updateTransaction: async (req, res) => {
        await LibraryController.response(req, res, async (body)=> {
            const result = await BnimodulController.updateTransaction(req.body);
            return result;
        });
    },

    sendNotifBniPost: async (req, res) => {
        const body = req.body;
        const result = await BnimodulController.sendNotif(body);
        console.log({check:"berhasil dari POST"});
        return await res.json({"status":result});
    },

    sendNotifBniGet: async (req, res) => {
        console.log({check: "berhasil dari GET"});
        return res.json({"status": "000"});
    }
    // END BNI

}

module.exports = TranscustController;

