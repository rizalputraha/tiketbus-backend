/**
 * SignupController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const LoginController = require('../controllers/auth/LoginController');
const SignupController = require('../controllers/auth/SignupController');

const UserController = {
    detailUser: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id_user } = res.locals.udata.payload;
            const resp = await User.find({id:id_user}).populate("crew").populate("agen");
            delete resp[0].pass_user;
            delete resp[0].pin;
            return resp;
        })
    },

    listUser: async function (req, res) {
      await LibraryController.response(req, res, async (body) => {
        const {email} = req.query;
        if (email) {
            const resp = await User.find({select: ['id', 'nm_user', 'addr_user', 'telp_user', 'email_user'], where: {status: 0, email_user:email}});
            // return UserController.hiddenPassword(resp);
            return resp;
        }
        const resp = await User.find({select: ['id', 'nm_user', 'addr_user', 'telp_user', 'email_user'], where: {status: 0}});
        // return UserController.hiddenPassword(resp);
        return resp;
      });
    },
    
    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await UserController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {tipe_user} = res.locals.udata.payload;
            let resp = await SignupController.signupProcess(req.body, tipe_user);
            return resp;
        })
    },

    updateProcess: async function (data, reqBody) {
        let hashed = "";
        let hashPin = "";
        let obj;
        if (reqBody.pass_user != undefined){
            hashed = LoginController.createSalt({email: reqBody.email_user}, reqBody.pass_user);
            obj = {...reqBody, pass_user: hashed};
        }

        if (reqBody.pin != undefined) {
            hashPin = SignupController.createHashPin(reqBody.pin);
            obj = {...reqBody, pin: hashPin};
        }

        if (reqBody.pass_user != undefined && reqBody.pin != undefined) {
            obj = {...reqBody, pass_user: hashed, pin: hashPin};
        } else {
            obj = {...reqBody};
        }
        
        const result = await User.updateOne({id: data}).set(obj);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await User.destroyOne({id: req.params.id});
            const emp = await Emplypo.destroyOne({id_user: req.params.id});
            return true;
        })
    }
}

module.exports = UserController;

