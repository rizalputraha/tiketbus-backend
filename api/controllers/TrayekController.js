/**
 * TrayekController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const TrayekController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await TrayekController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        const arrKota = body.kota;
        let resp = await Trayek.create(body).fetch().meta({skipRecordVerification: true});
        const arrPush = [];
        for (let i = 0; i < arrKota.length; i++) {
            const e = arrKota[i];
            const obj = {
                id_trayek:resp.id,
                id_kota:e,
                status:0
            }
            arrPush.push(obj);
        }
        const qry_kota_singgah = KotaSinggah.createEach(arrPush).fetch().meta({skipRecordVerification: true});
        return qry_kota_singgah;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id_user} = res.locals.udata.payload;
            let resp = await Trayek.find({}).populate('id_pobus').populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification: true});
            for (let i = 0; i < resp.length; i++) {
                const e = resp[i];
                const kotSing = await KotaSinggah.find({where : {id_trayek : e.id}}).populate('id_kota').meta({skipRecordVerification: true});
                e.kota_singgah = kotSing;
            }
            const result = resp.filter(k => k.id_pobus.id_user == id_user);
            return result.length === 0 ? 'Data tidak ditemukan' : result ;
            // return resp;
        });
    },

    listTrayekNoAuth: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {asal, tujuan} = req.query;
            let resp = await Trayek.find({}).populate('id_pobus').populate('id_kota_asal').populate('id_kota_tujuan');
            if (asal || tujuan) {
                const fill = resp.filter(e => e.id_kota_asal.nm_kota.toLowerCase() == asal.toLowerCase()
                .toLowerCase() && e.id_kota_tujuan.nm_kota.toLowerCase() == tujuan.toLowerCase());
                return fill
            }
            return resp;
        });
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await TrayekController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        // const id_trayek = data;
        const check_kota = reqBody.kota;
        const check_id_kota = reqBody.id_kota_singgah;
        const result = await Trayek.updateOne({id: data}).set(reqBody);
        if (check_kota || check_id_kota) {
            for (let i = 0; i < check_id_kota.length && check_kota.length; i++) {
                const e_id = check_id_kota[i];
                const k_id = check_kota[i];
                await KotaSinggah.updateOne({id: e_id}).set({id_kota: k_id});
            }
        }
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Trayek.destroyOne({id: req.params.id});
            return true;
        })
    },

    deleteKotaSinggah: async function(req,res) {
        await LibraryController.response(req,res, async(body) => {
            const resp = await KotaSinggah.destroyOne({id: req.params.id});
            return true;
        })
    },

    cekTrayek: async function(req, res, callback) {
        await LibraryController.response(req, res, async (body) => {
          let kAsal = req.param('asal');
          let kTujuan = req.param('tujuan');

          const asal = await Kota.findOne({nm_kota:kAsal}).meta({skipRecordVerification:true});
          const tujuan = await Kota.findOne({nm_kota:kTujuan}).meta({skipRecordVerification:true});

          const berangkat = await Trayek.find({id_kota_asal:asal.id, id_kota_tujuan:tujuan.id}).populate('jam_trayek').populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification:true});
          berangkat[0].harga = new Intl.NumberFormat(['ban', 'id']).format(berangkat[0].harga);
          const jtfilter = berangkat[0].jam_trayek.filter((v,i,a)=>a.findIndex(t=>(t.jam_berangkat === v.jam_berangkat))===i)
          berangkat[0].jam_trayek = jtfilter;
          const respon = {'berangkat':berangkat};
          return respon;
        });
      },
    
    pulangPergi: async function(req, res, callback) {
        await LibraryController.response(req, res, async (body) => {
            let kAsal = req.param('asal');
            let kTujuan = req.param('tujuan');
            
            const asal = await Kota.findOne({nm_kota:kAsal}).meta({skipRecordVerification:true});
            const tujuan = await Kota.findOne({nm_kota:kTujuan}).meta({skipRecordVerification:true});

            const berangkat = await Trayek.find({id_kota_asal:asal.id, id_kota_tujuan:tujuan.id}).populate('jam_trayek').populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification:true});
           
            const kembali = await Trayek.find({id_kota_asal:tujuan.id, id_kota_tujuan:asal.id}).populate('jam_trayek').populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification:true});;
            berangkat[0].harga = new Intl.NumberFormat(['ban', 'id']).format(berangkat[0].harga);
            const filterBerangkat = berangkat[0].jam_trayek.filter((v,i,a)=>a.findIndex(t=>(t.jam_berangkat === v.jam_berangkat))===i)
            const filterKembali = kembali[0].jam_trayek.filter((v,i,a)=>a.findIndex(t=>(t.jam_berangkat === v.jam_berangkat))===i)
            berangkat[0].jam_trayek = filterBerangkat;
            kembali[0].jam_trayek = filterKembali
            const respon = {'berangkat':berangkat, 'kembali':kembali};

            return respon;
        });
    }
}

module.exports = TrayekController;

