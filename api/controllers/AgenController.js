/**
 * AgenController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const AgenController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await AgenController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Agen.create(body);
        let updateTypeUser = await User.updateOne({id: body.id_user}).set({type_user: 3});
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id_user} = res.locals.udata.payload;
            let resp = await Agen.find({}).populate('id_pobus').populate('id_user');
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        });
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await AgenController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Agen.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Agen.destroyOne({id: req.params.id});
            return true;
        })
    }
}

module.exports = AgenController;

