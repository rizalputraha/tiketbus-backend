/**
 * PobusController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const PobusController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await PobusController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Pobus.create(body);
        let updateTypeUser = await User.updateOne({id: body.id_user}).set({type_user: 2});
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await Pobus.find({});
            return resp;
        });
    },

    listEmployer: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id_user} = res.locals.udata.payload;
            let resp = await Emplypo.find({}).populate('id_pobus').populate('id_user');
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        })
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await PobusController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Pobus.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Pobus.destroyOne({id: req.params.id});
            return true;
        })
    }
}

module.exports = PobusController;

