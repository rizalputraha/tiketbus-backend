/**
 * TransagenController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const moment = require('moment');
// const TrayekController = require('./TrayekController');
const NotificationController = require('../controllers/NotificationController');
const BrimodulController = require('../controllers/bank/BrimodulController');

const TransagenController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id_pobus, id_agen, id_jamtrayek, id_kursi, tanggal, nama, qty, status, total_hrg, no_telp, id_bus } = req.body;
            
            let tanggal_smp;
            const jt = await Jamtrayek.findOne({id:id_jamtrayek}).meta({skipRecordVerification:true});
            const start = moment(jt.jam_berangkat, "HH:mm");
            const end = moment(jt.jam_sampai, "HH:mm");
            if(end.diff(start) < 0 ) {
                tanggal_smp = moment().add(1, 'd').format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
            } else {
                tanggal_smp = moment().format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
            }

            const data = {
                id_pobus,
                kd_transaksi: 'AGN' + moment().format('DDMMYY') + id_agen + LibraryController.randomNumber(),
                id_agen,
                id_jamtrayek,
                id_kursi,
                id_bus,
                tanggal,
                tanggal_smp,
                nama,
                qty,
                status,
                total_hrg,
                no_telp,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
            }
            const maxOrder = await Agen.findOne({where : {id: id_agen}});
            if (maxOrder.max_order === 0) {
                throw new Error('Sudah melakukan order lebih dari 4x, harap hubungi CS');
            }
            let resp = await TransagenController.createProcess(data, maxOrder);
            if (!resp) {
                throw new Error('Transaksi Gagal');
            }else{
                const updateKursi = await Kursi.updateOne({ id: id_kursi }).set({ available: 1 });
                return resp;
            }
        })
    },

    createProcess: async function (body, maxOrder) {
        const jt = await Jamtrayek.find({id_bus:body.id_bus}).meta({skipRecordVerification:true});
        const jt1 = await Jamtrayek.findOne({id:body.id_jamtrayek}).populate("id_trayek").meta({skipRecordVerification:true});
        let p;
        let b;
        if(jt1.id_trayek.keterangan == "Timur-Barat") {
            p = await Kota.findOne({"nm_kota":"Surabaya"});
        } else {
            p = await Kota.findOne({"nm_kota":"Cimahi"});
        }

        for(i=0;i<jt.length;i++) {
            const a = await Trayek.findOne({id:jt[i].id_trayek}).meta({skipRecordVerification:true});
            console.log(i)
            console.log(a)
            if(a.id_kota_asal == p.id) {
                console.log(jt[i])
                b = moment(jt[i].jam_berangkat,"HH:mm").format('YYYY-MM-DD HH:mm:ss').valueOf()
                break;
            }
        }

        const now = moment().format('YYYY-MM-DD HH:mm:ss').valueOf();
        if (moment(now).diff(moment(b)) > 0) {
            const updateKursi = await Kursi.updateOne({ id: body.id_kursi }).set({ available: 1 });
            let resp = await Transagen.create(body).fetch();
            if (resp) {
                await Agen.updateOne({id: maxOrder.id}).set({max_order: maxOrder.max_order-1});
            }
            const { id_bus } = resp;
            const players_id_arr = [];
            const dataBus = await Checkinbus.find({ where: { id_bus: id_bus, logout: null } }).populate('id_crew');
            const filterCrew = dataBus.filter(e => e.id_crew);
            const checkUser = await TransagenController.findUser(filterCrew);
            for (let i = 0; i < checkUser.length; i++) {
                const e = checkUser[i].id_crew.user.player_id;
                players_id_arr.push(e);
            }
            // console.log({players_id_arr});
            const message = {
                app_id: sails.config.custom.app_id,
                contents: { "en": "Ada penumpang baru sedang menunggu", "id": "Ada penumpang baru sedang menunggu" },
                data: { "key":5 },
                // included_segments: ["All"]
                include_player_ids: players_id_arr
            };
            const logNotif = await NotificationController.notifProcess(message);
            return resp;
        } else {
            throw new Error("Tidak boleh memesan tiket sebelum jam keberangkatan");
        }
        
    },

    findUser: async function (arrCrew) {
        const resUser = await User.find({});
        for (let i = 0; i < arrCrew.length; i++) {
            const eCrew = arrCrew[i];
            const fill = resUser.filter(e => e.id == eCrew.id_crew.id_user)[0];
            eCrew.id_crew.user = fill;
        }
        return await arrCrew;
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await TransagenController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Transagen.updateOne({ id: data }).set(reqBody);
        return true;
    },

    updateLunas: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await TransagenController.updateLunasProcess(req.params.id);
            return proc;
        });
    },

    updateLunasProcess: async function (id) {
        const result = await Transagen.updateOne({ id: id }).set({ status: 1 });
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id_user, tipe_user } = res.locals.udata.payload;
            let result = null;

            let resp = await Transagen.find({}).populate('id_agen').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate('id_bus').meta({skipRecordVerification: true});

            if (tipe_user == 3) {
                result = resp.filter(e => e.id_agen.id_user == id_user);
            } else if (tipe_user == 2) {
                result = resp.filter(e => e.id_pobus.id_user == id_user);
            }
            for (let i = 0; i < result.length; i++) {
                let el = result[i];
                el.id_jamtrayek.id_trayek = await Trayek.findOne({ id: el.id_jamtrayek.id_trayek }).meta({skipRecordVerification: true});
                const resKotaAsal = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_asal }).meta({skipRecordVerification: true});
                const resKotaTujuan = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_tujuan }).meta({skipRecordVerification: true});
                el.id_jamtrayek.id_trayek.k_asal = resKotaAsal.nm_kota;
                el.id_jamtrayek.id_trayek.k_tujuan = resKotaTujuan.nm_kota;
            }
            
            return result;
        });
    },

    detail: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {kursi} = req.query;
            // let result = null;
            
            if (!kursi) {
                return 'Kursi tidak ditemukan';
            }
            let respAgen = await Transagen.findOne({where: {id_kursi: kursi}}).populate('id_bus').populate('id_agen').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').meta({skipRecordVerification: true});
            let respCrew = await Transcrew.findOne({where: {id_kursi: kursi}}).populate('id_bus').populate('id_crew').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').meta({skipRecordVerification: true});
            
            const arrResp = [respAgen, respCrew];
            const sortArr = arrResp.sort((a,b) => {return a.tanggal - b.tanggal})[0];
            sortArr.id_jamtrayek.trayek = await Trayek.findOne({id: sortArr.id_jamtrayek.id_trayek}).populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification: true});

            return sortArr;
        });
    },

    historyPo: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {startDate, endDate} = req.query;
            let resp;
            if (startDate && endDate) {
                resp = await Transagen.find({tanggal: { '>=': startDate, '<=': endDate }}).sort('tanggal ASC').populate('id_agen').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate("id_bus");
            }else{
                resp = await Transagen.find({}).populate('id_agen').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate("id_bus");
                for (let i = 0; i < resp.length; i++) {
                    let el = resp[i];
                    el.id_jamtrayek.id_trayek = await Trayek.findOne({ id: el.id_jamtrayek.id_trayek });
                    const resKotaAsal = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_asal });
                    const resKotaTujuan = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_tujuan });
                    el.id_jamtrayek.id_trayek.k_asal = resKotaAsal.nm_kota;
                    el.id_jamtrayek.id_trayek.k_tujuan = resKotaTujuan.nm_kota;
                }
            }
            return resp;
        });
    },

    calonPenumpang: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { bus } = req.query;
            let obj = {};
            let arr = [];
            const resp = await Transagen.find({ where: { status: 1, tanggal: moment().format('YYYY-MM-DD') } }).populate('id_agen').populate('id_jamtrayek').populate('id_bus');
            const res = resp.filter(e => e.id_bus.id == bus);
            for (let i = 0; i < res.length; i++) {
                const e = res[i];
                obj = {
                    id: e.id,
                    tanggal: e.tanggal,
                    nama: e.nama,
                    notelp: e.no_telp,
                    lokasi: e.id_agen,
                    trayek: e.id_jamtrayek,
                }
                arr.push(obj);
            }
            return arr;
        })
    },

    konfirmasiKursi: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { kode } = req.params;
            const result = await Transagen.find({ where: { kd_transaksi: kode } }).populate('id_kursi').populate('id_jamtrayek');
            return result;
        })
    },

}

module.exports = TransagenController;

