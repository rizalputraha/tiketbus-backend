/**
 * BankController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const LibraryController = require('../controllers/LibraryController');

const BankController = {
    read: async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const result = await Bank.find({});
            return result
        })
    }
}

module.exports = BankController;

