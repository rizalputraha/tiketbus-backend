/**
 * JamtrayekController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const JamtrayekController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await JamtrayekController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Jamtrayek.create(body);
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id} = req.query;
            let bus = await Bus.find({"id":id}).meta({skipRecordVerification:true});
            
            let resp = await Jamtrayek.find({'id_bus':id}).populate('id_bus').populate('id_trayek').meta({skipRecordVerification:true});
            const hasilTrayek = await JamtrayekController.searchKota(resp);
            const result = resp.filter(e => e.id_bus.id == id && e.id_trayek.keterangan == bus[0].keterangan);
            // return hasilTrayek;
            return id ? result : hasilTrayek;
        });
    },

    readV2: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {jam_berangkat, jam_sampai} = req.query;
            if (!jam_berangkat && !jam_sampai) {
                throw new Error("Jam berangkat & jam sampai must be inquired!");
            };
            let resp = await Jamtrayek.find({
                jam_berangkat: { '<=' : jam_berangkat },
                jam_sampai: { '>=' : jam_sampai },
            }).populate('id_bus').populate('id_trayek').meta({skipRecordVerification:true});
            const hasilTrayek = await JamtrayekController.searchKota(resp);
            // return id ? result : hasilTrayek;
            return hasilTrayek;
        });
    },

    readById: async function (req,res) {
        await LibraryController.response(req, res, async (body) => {
            const {id} = req.params;
            let resp = await Jamtrayek.find({id:id}).populate('id_bus').populate('id_trayek').meta({skipRecordVerification:true});
            const hasilTrayek = await JamtrayekController.searchKota(resp);
            return hasilTrayek;
        });
    },

    searchKota: async function (jamTrayek) {
        const kota = await Kota.find({}).meta({skipRecordVerification:true});
        for (let i = 0; i < jamTrayek.length; i++) {
            const ejt = jamTrayek[i];
            const resultAsal = kota.find(e => e.id == ejt.id_trayek.id_kota_asal);
            const resultTujuan = kota.find(e => e.id == ejt.id_trayek.id_kota_tujuan);
            ejt.id_trayek.k_asal = resultAsal.nm_kota;
            ejt.id_trayek.k_tujuan = resultTujuan.nm_kota;
        }
        return jamTrayek;
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await JamtrayekController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Jamtrayek.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Jamtrayek.destroyOne({id: req.params.id});
            return true;
        })
    },
}

module.exports = JamtrayekController;