/**
 * TiketController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
// let moment = require('moment');
let moment = require('moment-timezone');
moment().tz('Indonesia/Jakarta').format();

const TiketController = {
    checkTiketV2: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {tanggal, asal, tujuan} = req.query;
            let trayekFil;
            const tglnow = moment().format("yyyy-MM-DD").toString()
            const ch = parseInt(tanggal.substring(8)) - parseInt(tglnow.substring(8));
            const jamSekarang = moment().format("HH:mm");
            // const dateSekarang = moment().format('DD-MM-YYYY');

            // const dateSekarang2 = moment().toString();
            const resTanggal = moment(moment(tanggal));
            // const resU = resTanggal.diff(dateSekarang2, 'minutes'); // mencari perselisihan menit
            const resDays = moment(resTanggal).startOf('day').diff( moment().startOf('day'), 'days', true); // mencari perselisihan hari

            let bus;
            
            const kAsal = await Kota.findOne({where:{nm_kota: asal}}).meta({skipRecordVerification: true});
            const kTujuan = await Kota.findOne({where:{nm_kota: tujuan}}).meta({skipRecordVerification: true});
            const trayek = await Trayek.find({where:{id_kota_asal:kAsal.id, id_kota_tujuan:kTujuan.id}}).populate('jam_trayek').populate('id_kota_asal').populate('id_kota_tujuan').populate('id_pobus').meta({skipRecordVerification: true});
            var ket = trayek[0].keterangan;

            if (ch == 1) {
                if (ket == "Barat-Timur") {
                    ket = "Timur-Barat";
                } else if (ket == "Timur-Barat") {
                    ket = "Barat-Timur";
                }
            };

            bus = await Bus.find({where : {status: 0, keterangan: ket}}).populate('kursi').meta({skipRecordVerification: true}); // status 0 = active & 1 = cadangan
            // const startEvt = moment(trayek[0].start_tgl_event).format('YYYY-MM-DD');
            // const endEvt = moment(trayek[0].end_tgl_event).format('YYYY-MM-DD');
            // const checkVal = moment(tanggal).isBetween(startEvt, endEvt, null, '[]');
            const checkEvt = trayek.filter(e => moment(tanggal).isBetween(moment(e.start_tgl_event).format('YYYY-MM-DD'), moment(e.end_tgl_event).format('YYYY-MM-DD'), null, '[]'));
            if (checkEvt.length == 0) {
                trayekFil = trayek.filter(e => e.start_tgl_event == null);
            } else {
                trayekFil = checkEvt;
            }
            // console.log({trayekFil});
            for (let it = 0; it < trayekFil.length; it++) {
                const et = trayekFil[it];
                const checkTrayek = ket;
                const datePars = moment(tanggal).format('YYYY/MM/DD');
                const sort = et.jam_trayek.sort((a, b) => {return Date.parse(datePars+' ' + a.jam_berangkat.slice(0, -2) + ' ' + a.jam_berangkat.slice(-2)) - Date.parse(datePars+' ' + b.jam_berangkat.slice(0, -2) + ' ' + b.jam_berangkat.slice(-2))});
                const filJam = TiketController.selectionHours(sort, jamSekarang, resDays);
                if (filJam.length == 0) {
                    throw new Error('Tiket tidak ditemukan');
                }
                for (let ijt = 0; ijt < filJam.length; ijt++) {
                    const ejt = filJam[ijt];
                    ejt.jam_berangkat = moment(ejt.jam_berangkat).format('HH:mm');
                    const merge = bus.filter(e => e.id == ejt.id_bus);
                    const lookKeterangan = merge.filter(i => i.keterangan.toLowerCase() == checkTrayek.toLowerCase());
                    ejt.bus = lookKeterangan;
                }
                const ccd = filJam.filter(e => e.bus.length != 0);
                et.jam_trayek = ccd;
            };
            return trayekFil;
            
        })
    }, 

    checkTiketNoAuth: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {tanggal, asal, tujuan} = req.query;
            let trayekFil;
            const tglnow = moment().format("yyyy-MM-DD");
            const ch = parseInt(tanggal.substring(8)) - parseInt(tglnow.substring(8));
            const jamSekarang = moment().format("HH:mm");
            // const dateSekarang = moment().format('DD-MM-YYYY');
            
            // const dateSekarang2 = moment().toString();
            const resTanggal = moment(moment(tanggal));
            // const resU = resTanggal.diff(dateSekarang2, 'minutes'); // mencari perselisihan menit
            const resDays = moment(resTanggal).startOf('day').diff( moment().startOf('day'), 'days', true); // mencari perselisihan hari

            let bus;
        
            const kAsal = await Kota.findOne({where:{nm_kota: asal}}).meta({skipRecordVerification: true});
            const kTujuan = await Kota.findOne({where:{nm_kota: tujuan}}).meta({skipRecordVerification: true});
            // console.log({kAsal, kTujuan});
            const trayek = await Trayek.find({where:{id_kota_asal:kAsal.id, id_kota_tujuan:kTujuan.id}}).populate('jam_trayek').populate('id_kota_asal').populate('id_kota_tujuan').populate('id_pobus').meta({skipRecordVerification: true});
            // console.log({trayek});
            var ket = trayek[0].keterangan;

            if (ch == 1) {
                if (ket == "Barat-Timur") {
                    ket = "Timur-Barat";
                } else if (ket == "Timur-Barat") {
                    ket = "Barat-Timur";
                }
            };
            
            bus = await Bus.find({where : {status: 0, keterangan: ket}}).populate('kursi').meta({skipRecordVerification: true}); // status 0 = active & 1 = cadangan
            const checkEvt = trayek.filter(e => moment(tanggal).isBetween(moment(e.start_tgl_event).format('YYYY-MM-DD'), moment(e.end_tgl_event).format('YYYY-MM-DD'), null, '[]'));
            if (checkEvt.length == 0) {
                trayekFil = trayek.filter(e => e.start_tgl_event == null);
            } else {
                trayekFil = checkEvt;
            }
            for (let it = 0; it < trayekFil.length; it++) {
                const et = trayekFil[it];
                const checkTrayek = ket;
                const datePars = moment(tanggal).format('YYYY/MM/DD');
                const sort = et.jam_trayek.sort((a, b) => {return Date.parse(datePars+' ' + a.jam_berangkat.slice(0, -2) + ' ' + a.jam_berangkat.slice(-2)) - Date.parse(datePars+' ' + b.jam_berangkat.slice(0, -2) + ' ' + b.jam_berangkat.slice(-2))});
                const filJam = TiketController.selectionHours(sort, jamSekarang, resDays);
                if (filJam.length == 0) {
                    throw new Error('Tiket tidak ditemukan');
                }
                for (let ijt = 0; ijt < filJam.length; ijt++) {
                    const ejt = filJam[ijt];
                    ejt.jam_berangkat = moment(ejt.jam_berangkat).format('HH:mm');
                    const merge = bus.filter(e => e.id == ejt.id_bus);
                    const lookKeterangan = merge.filter(i => i.keterangan.toLowerCase() == checkTrayek.toLowerCase());
                    ejt.bus = lookKeterangan;
                }
                const ccd = filJam.filter(e => e.bus.length != 0);
                et.jam_trayek = ccd;
            };
            return trayekFil;
            
        })
    },

    selectionHours: function (arr, jamSekarang, resDays) {
        for (let i = 0; i < arr.length; i++) {
            const e = arr[i];
            e.jam_berangkat = moment(e.jam_berangkat,'HHmm');
        }
        // const fill = dateSekarang < tanggal ? arr : arr.filter(a => a.jam_berangkat.format('HH:mm') > jamSekarang);
        // console.log({fill});
        if (resDays >= 1) {
            return arr;
        } else if (resDays == 0) {
            const res = arr.filter(a => a.jam_berangkat.format('HH:mm') > jamSekarang);
            return res;
        } else if (resDays < 0) {
            return []
        }
        // return fill;
    }
}

module.exports = TiketController;

