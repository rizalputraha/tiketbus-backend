/**
 * BnimodulController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const LibraryController = require('../LibraryController');
const BniEncrypt = require('../../../BniEncryption');
const env = require('../../../env');
const url = 'https://api.bni-ecollection.com';
let cid = '81115';
let prefix = "988";
let secret_key = env.secret_key_bni;

const BnimodulController = {
    createInvoiceBilling: async (reqBody) => {
        let two_hours = new Date(+new Date() + 0.25 * 3600 * 1000);
        reqBody.client_id = cid;
        reqBody.datetime_expired = two_hours.toISOString();

        let encrypt_string = BniEncrypt.encrypt(reqBody, cid, secret_key);

        const data = {
            "client_id": cid,
            "prefix": prefix,
            "data": encrypt_string
        }
        
        const headers = {"Content-Type": "application/json"}
        const body = {url,data,headers}

        // let parse_string = BniEncrypt.decrypt(reqBody, cid, secret_key);

        const sendRequest = await LibraryController.methodPost(body);
        if (sendRequest.status != '000') {
            throw new Error(sendRequest.message);
        }
        const decrypt = BniEncrypt.decrypt(sendRequest.data, cid, secret_key);
        sendRequest.decrypt = decrypt
        return sendRequest;
    },

    checkInquiryBilling: async (reqBody) => {
        reqBody.client_id = cid;
        let encrypt_string = BniEncrypt.encrypt(reqBody, cid, secret_key);
        const data = {
            "client_id":cid,
            "prefix": prefix,
            "data":encrypt_string
        };
        const headers = {"Content-Type": "application/json"}
        const body = {url,data,headers};
        const sendRequest = await LibraryController.methodPost(body);
        if (sendRequest.status != '000') {
            throw new Error(sendRequest.message);
        }
        const decrypt = BniEncrypt.decrypt(sendRequest.data, cid, secret_key);
        sendRequest.decrypt = decrypt
        return sendRequest;
    },
    
    updateTransaction: async (reqBody) => {
        let two_hours = new Date(+new Date() + 2 * 3600 * 1000);
        reqBody.client_id = cid;
        reqBody.datetime_expired = two_hours.toISOString();
        let encrypt_string = BniEncrypt.encrypt(reqBody, cid, secret_key);
        const data = {
            "client_id":cid,
            "prefix": prefix,
            "data":encrypt_string
        };
        const headers = {"Content-Type": "application/json"}
        const body = {url,data,headers};
        const sendRequest = await LibraryController.methodPost(body);
        if (sendRequest.status != '000') {
            throw new Error(sendRequest.message);
        }
        const decrypt = BniEncrypt.decrypt(sendRequest.data, cid, secret_key);
        sendRequest.decrypt = decrypt
        return sendRequest;
    },

    sendNotif: async (body) => {
        const decr = BniEncrypt.decrypt(body.data, cid, secret_key);
        console.log({decr});
        if (decr != null) {
            if (!decr.trx_id || !decr.virtual_account || !decr.customer_name || !decr.trx_amount || !decr.payment_amount || !decr.payment_ntb || !decr.datetime_payment || !decr.datetime_payment_iso8601) {
                return await "001";
            }
            const create = await Notifbni.create(decr);
            return "000";
        }
        return await "001";
    }
}

module.exports = BnimodulController;

