/**
 * BcamodulController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../LibraryController');
let moment = require('moment');
let crypto = require('crypto');
const env = require('../../../env');
const baseUrl = "https://api.klikbca.com";
const origin = "https://perusahaanotobus.com";
let TOKEN;
let TOKEN2;

const BcamodulController = {
    createSignature: (apiSecret, stringToSign) => {
        stringToSign = stringToSign.replace(/,|_/g,"%2C");
        const sign = crypto.createHmac('sha256', apiSecret).update(stringToSign).digest('hex');
        return sign;
    },

    generateSign: (httpMethod, relativeUrl, accessToken, requestBody, timestampIso, tipe) => {
        let body = httpMethod == "GET" ? requestBody : JSON.stringify(requestBody);
        // const lowercase = body.toLowerCase();
        const makeEncode = crypto.createHash('sha256').update(body.replace(/\s/g, '')).digest('hex');
        const stringToSign = httpMethod+":"+encodeURI(relativeUrl)+":"+accessToken+":"+makeEncode+":"+timestampIso;
        const secretKey = tipe == "inquiry" ? env.secret_key_bca : env.secret_key_saldo_bca;
        const signature = BcamodulController.createSignature(secretKey, stringToSign);
        return signature;
    },

    // CREDENTIALS
    BcaCredentials: async (reqBody) => {
        const url = `${baseUrl}/api/oauth/token`;
        const createAuth = Buffer.from(env.client_id_bca+':'+env.client_secret_bca).toString('base64');
        const headers = {'Content-Type':'application/x-www-form-urlencoded', 'Authorization': `Basic ${createAuth}`};
        const data = reqBody;
        const body = {url, data, headers};
        const response = await LibraryController.methodPostCred(body);
        TOKEN = response.access_token;
        return response;
    },

    BcaCredentials2: async (reqBody) => {
        const url = `${baseUrl}/api/oauth/token`;
        const createAuth = Buffer.from(env.client_id_saldo_bca+':'+env.client_secret_saldo_bca).toString('base64');
        const headers = {'Content-Type':'application/x-www-form-urlencoded', 'Authorization': `Basic ${createAuth}`};
        const data = reqBody;
        const body = {url, data, headers};
        const response = await LibraryController.methodPostCred(body);
        TOKEN2 = response.access_token;
        console.log({response});
        return response;
    },

    // BILLS ???
    bcaBills: async (reqBody) => {
        const relUrl = "/va/bills";
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "inquiry";
        const resSignature = BcamodulController.generateSign('POST', relUrl, TOKEN, reqBody, timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = reqBody;
        const body = {url, data, headers};
        const response = await LibraryController.methodPost(body);
        return response;
        // return true;
    },

    // PAYMENTS ??
    bcaPayments: async (reqBody) => {
        const relUrl = "/va/payments";
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "inquiry";
        const resSignature = BcamodulController.generateSign('POST', relUrl, TOKEN, reqBody, timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = reqBody;
        const body = {url, data, headers};
        const response = await LibraryController.methodPost(body);
        return response;
        // return true;
    },

    // INQUIRY PAYMENTS
    inquiryPayment: async (company, customer) => {
        const relUrl = `/va/payments?CompanyCode=${company}&CustomerNumber=${customer}`;
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "inquiry";
        const resSignature = BcamodulController.generateSign('GET', relUrl, TOKEN, '', timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = '';
        const body = {url, data, headers};
        const response = await LibraryController.methodGet(body);
        return response;
    },

    inquiryPaymentReqId: async (company, reqId) => {
        const relUrl = `/va/payments?CompanyCode=${company}&RequestID=${reqId}`;
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "inquiry";
        const resSignature = BcamodulController.generateSign('GET', relUrl, TOKEN, '', timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = '';
        const body = {url, data, headers};
        const response = await LibraryController.methodGet(body);
        return response;
    },
    
    // BANKING SALDO
    bankingSaldo: async (company, customer) => {
        const relUrl = `/banking/v3/corporates/${company}/accounts/${customer}`;
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "saldo";
        const resSignature = BcamodulController.generateSign('GET', relUrl, TOKEN2, '', timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN2}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_saldo_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = '';
        const body = {url, data, headers};
        const response = await LibraryController.methodGet(body);
        return response;
    },

    // BANKING MUTASI
    mutasiBanking: async (company, customer, start, end) => {
        const relUrl = `/banking/v3/corporates/${company}/accounts/${customer}/statements?EndDate=${moment(end).format('YYYY-MM-DD')}&StartDate=${moment(start).format('YYYY-MM-DD')}`;
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "saldo";
        const resSignature = BcamodulController.generateSign('GET', relUrl, TOKEN2, '', timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN2}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_saldo_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = '';
        const body = {url, data, headers};
        const response = await LibraryController.methodGet(body);
        return response;
    },

    // ONLINE TRANSFER
    onlineTransfer: async (reqBody) => {
        reqBody.TransactionDate = moment().format('YYYY-MM-DD');
        const relUrl = `/banking/corporates/transfers`;
        const url = `${baseUrl+relUrl}`;
        const timestampIso = new Date().toISOString();
        const tipe = "saldo";
        const resSignature = BcamodulController.generateSign('POST', relUrl, TOKEN2, reqBody, timestampIso, tipe);
        const headers = {'Authorization': `Bearer ${TOKEN2}`, 'Content-Type': 'application/json', 'Origin': origin, 'X-BCA-Key': env.api_key_saldo_bca, 'X-BCA-Timestamp': timestampIso, 'X-BCA-Signature': resSignature};
        const data = '';
        const body = {url, data, headers};
        const response = await LibraryController.methodPost(body);
        return response;
    }

}

module.exports = BcamodulController;

