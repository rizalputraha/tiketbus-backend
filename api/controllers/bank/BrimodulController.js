/**
 * BrimodulController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const LibraryController = require('../LibraryController');
const CryptoJS = require('crypto-js');
const baseUrl = 'https://partner.api.bri.co.id/';
const env = require('../../../env');

const BrimodulController = {
    getPath : (url) => {
        var pathRegex = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        var result = url.match(pathRegex);
        return result && result.length > 1 ? result[1] : '';
    },

    getQueryString: (url) => {
        var arrSplit = url.split('?');
        return arrSplit.length > 1 ? url.substring(url.indexOf('?')+1) : ''; 
    },

    generateSignature : (httpMethod, requestUrl, requestBody, token, secret_key) => {
        // console.log({httpMethod, requestUrl, requestBody, token, secret_key});
        var requestPath = BrimodulController.getPath(requestUrl);
        var queryString = BrimodulController.getQueryString(requestUrl);
        
        if (httpMethod == 'GET' || !requestBody) {
            requestBody = ''; 
        } else {
            requestBody = requestBody;
        }
        
        var timestamp = new Date().toISOString();
        let chec = !requestBody ? '' : JSON.stringify(requestBody);
        if (httpMethod == 'DELETE') {
            chec = requestBody;
        }
        // console.log({chec});
        
        payload = 'path=' + requestPath + '&verb=' + httpMethod + '&token=Bearer '+ token + 
                '&timestamp=' + timestamp + '&body=' + chec;

                
        var hmacSignature = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(payload, secret_key));
        return {hmacSignature, timestamp};
    },

    // CREDENTIALS
    briClientCredential : async (client_id, client_secret, params) => {
        const url = `${baseUrl}oauth/client_credential/accesstoken?grant_type=${params}`;
        const data = {client_id, client_secret};
        const headType = {'Content-Type':'application/x-www-form-urlencoded'};
        const body = {url, data, headType};
        const checkEndpoint = await LibraryController.methodPostCred(body);
        return checkEndpoint;
    },

    // CREATE BRIVA (VIRTUAL ACCOUNT)
    briCreateVirtualAccount : async (reqBody, auth) => {
        const {institutionCode, brivaNo, custCode, nama, amount, keterangan, expiredDate} = reqBody;
        const url = `${baseUrl}v1/briva`;
        const data = {
            "institutionCode": institutionCode,
            "brivaNo": brivaNo,
            "custCode": custCode,
            "nama": nama,
            "amount": amount,
            "keterangan": !keterangan ? "" : keterangan,
            "expiredDate": expiredDate //format: yyyy-MM-dd HH:mm:ss
        }
        const headType = 'application/json';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('POST', url, data, sbToken, env.client_secret_bri);
        // console.log({getSignatur});
        const headers = { 'Content-Type': headType, 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodPost(body);
        return checkEndpoint;
    },

    // GET VIRTUAL ACCOUNT INFO
    getVirtualAccountInfo : async (params, auth) => {
        const {institution_code, briva_no, customer_code} = params;
        if (institution_code == 0) {
            throw new Error('Institution Code Tidak Boleh Kosong');
        }
        const data="";
        const url = `${baseUrl}v1/briva/${institution_code}/${briva_no}/${customer_code}`;
        // const headType = 'application/json';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('GET', url, data, sbToken, env.client_secret_bri);
        const headers = { 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodGet(body);
        return checkEndpoint;
    },

    getVirtualAccountStatus : async (params, auth) => {
        const {institution_code, briva_no, customer_code} = params;
        if (institution_code == 0) {
            throw new Error('Institution Code Tidak Boleh Kosong');
        }
        const data="";
        const url = `${baseUrl}v1/briva/status/${institution_code}/${briva_no}/${customer_code}`;
        // const headType = 'application/json';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('GET', url, data, sbToken, env.client_secret_bri);
        const headers = { 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodGet(body);
        return checkEndpoint;
    },

    // UPDATE STATUS PEMBAYARAN ACCOUNT VA
    updStatusPayVa : async (data, auth) => {
        const url = `${baseUrl}v1/briva/status`;
        const headType = 'application/json';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('PUT', url, data, sbToken, env.client_secret_bri);
        const headers = {'Content-Type': headType, 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodPut(body);
        return checkEndpoint;
    },

    updateVa : async (data, auth) => {
        const url = `${baseUrl}v1/briva`;
        const headType = 'application/json';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('PUT', url, data, sbToken, env.client_secret_bri);
        const headers = {'Content-Type': headType, 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodPut(body);
        return checkEndpoint;
    },

    // GET REPORT
    getReportVa: async (param, auth) => {
        const { institutionCode,brivaNo,startDate, endDate } = param;
        if (institutionCode == 0) {
            throw new Error('Institution Code Tidak Boleh Kosong');
        }
        const data = "";
        const url = `${baseUrl}v1/briva/report/${institutionCode}/${brivaNo}/${startDate}/${endDate}`;
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('GET', url, data, sbToken, env.client_secret_bri);
        const headers = {'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodGet(body);
        return checkEndpoint;
    },

    // DELETE VA
    deleteVa: async (data, auth) => {
        const url = `${baseUrl}v1/briva`;
        const headType = 'text/plain';
        const sbToken = auth.substring(7);
        const getSignatur = BrimodulController.generateSignature('DELETE', url, data, sbToken, env.client_secret_bri);
        const headers = {'Content-Type': headType, 'Authorization' : auth,'BRI-Timestamp': getSignatur.timestamp, 'BRI-Signature': getSignatur.hmacSignature};
        const body = {url, data, headers};
        const checkEndpoint = await LibraryController.methodDelete(body);
        // console.log({checkEndpoint});
        return checkEndpoint;
    }
}

 module.exports = BrimodulController;

