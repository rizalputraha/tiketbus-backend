/**
 * LibraryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var admin = require("firebase-admin");
var nodemailer = require('nodemailer');
var fs = require('fs');
var ejs = require('ejs');
var moment = require('moment');
var serviceAccount = require("../../service_account_file.json");
const SignupController = require('../controllers/auth/SignupController');
const axios = require('axios');
var qs = require('qs');
const env = require("../../env");
const wkhtmltopdf = require('wkhtmltopdf');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    projectId: "e-ticket-289307"
});

const LibraryController = {
    response: async function (req, res, callback) {
        let jres = {
            error: 0,
            data: [],
            message: '',
            stack: {},
            errorName: ''
        }
        try {
            jres.data = await callback(req.body)
        } catch (error) {
            jres.error = error.name == "UsageError" ? 400 : 500
            jres.message = error.name == "UsageError" ? "Bad Request!" : error.message
            jres.stack = error.stack
            jres.errorName = error.name
        }
        res.json(jres)
    },

    randomNumber: function () {
        return Math.floor(10000 + Math.random() * 90000);
    },

    sliceNumberString: function (id) {
        let resA = '';
        let resB = '';
        let objA = {};
        let arr = [];
        for (let ia = 1; ia < 19; ia++) {
            const data = ('0' + ia).slice(-2);
            resA = "A"+data;
            objA = {
                id_bus : id,
                kd_kursi : resA,
                available : 0
            }
            arr.push(objA)
        }
        for (let ib = 1; ib < 23; ib++) {
            const data = ('0' + ib).slice(-2);
            resB = "B"+data;
            objB = {
                id_bus : id,
                kd_kursi : resB,
                available : 0
            }
            arr.push(objB)
        }
        // console.log({arr});
        return arr;
    },

    createUserFirebase: async function (obj) {
        obj.email = obj.email_user;
        obj.displayName = obj.nm_member;
        // const abc = SignupController.createSalt({ email: obj.email_user }, obj.pass_user);
        obj.password = obj.pass_user;

        const result = await admin.auth().createUser(obj).then((userRecord) => {
            return userRecord.uid;
        }).catch(e => {
            throw new Error(e);
        });
        return result;
    },

    updateUserFirebase: async function (uid, obj) {
        obj.email = obj.email_user;
        obj.displayName = obj.nm_member;
        obj.password = obj.pass_user;
        const result = await firebaseService.auth().updateUser(uid, obj).then((userRecord) => {
            return userRecord;
        }).catch(e => {
            // console.log({err : e});
            throw new Error(e);
        });
        return result;
    },

    deleteUserFirebase: async function (uid) {
        const result = await firebaseService.auth().deleteUser(uid).then(() => {
            return "Successfully deleted user";
        }).catch(e => {
            // console.log({err : e});
            throw new Error(e);
        });
        return result;
    },

    callerEndpoint : async (method, url, data={}, headers={}, options={}) => {
        const config = {
            ...options,
            method,
            url,
            data,
            headers: {...headers}
        }
        const res = await axios(config).then(function (response) {
            // console.log(JSON.stringify(response.data));
            return response.data;
        }).catch(function(error) {
            // console.log({error: error.response.data});
            return error.response.data;
        });
        return res;
    },

    methodGet : async (body) => {
        const {url, data, headers} = body;
        // console.log({data});
        // const params = JSON.stringify(data);
        return await LibraryController.callerEndpoint('GET', url, data, headers);
    },

    methodPostCred : async (body) => {
        const {url, data, headers} = body;
        const parmData = qs.stringify(data);
        return await LibraryController.callerEndpoint('POST', url, parmData, headers);
    },

    methodPost : async (body) => {
        const {url, data, headers} = body;
        const parmData = JSON.stringify(data);
        return await LibraryController.callerEndpoint('POST', url, parmData, headers);
    },

    methodPut : async (body) => {
        const {url, data, headers} = body;
        const parmData = JSON.stringify(data);
        return await LibraryController.callerEndpoint('PUT', url, parmData, headers);
    },

    methodDelete : async (body) => {
        const {url, data, headers} = body;
        const parmData = data;
        return await LibraryController.callerEndpoint('DELETE', url, parmData, headers);
    },

    compileTemplate: (view,data) => {
        var compileFn = ejs.compile((fs.readFileSync(view+'.ejs') || "").toString(), {
        cache: true, filename: view
        });
        
        return compileFn(data);
    },

    sendEmail : async (body) => {
        let transport = nodemailer.createTransport({
            host: env.smtp_host,
            port: env.port,
            auth: {
               user: env.smtp_user,
               pass: env.smtp_pass
            }
        });
        body = {
            ...body,
            created_at: moment(body.created_at).format('dddd, DD MMMM yyyy HH:mm'),
            due_date: moment(body.due_date).format('dddd, DD MMMM yyyy HH:mm'),
            total: 'Rp ' + body.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        }
        const message = {
            from: env.smtp_user, // Sender address
            to: body.id_member.email_user, // List of recipients
            subject: 'Menunggu Pembayaran Eka Tiketbus', // Subject line
            html: LibraryController.compileTemplate("api/emailTemplate" + "/html", body)
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
              console.log(err)
            } else {
              console.log(info);
            }
        });
    },

    pdf: async function (req,res) {
        const {htmlString,fileName} = req.body;
        res.set('Content-Type', 'application/octet-stream;charset=utf-8');
        res.set('Content-disposition',`attachment;filename=${fileName}.pdf`);

        wkhtmltopdf(htmlString, {
            pageSize: 'A4',
            marginLeft: '1mm',
            marginTop: '1mm'
        }).pipe(res)
    }
}

module.exports = LibraryController;

