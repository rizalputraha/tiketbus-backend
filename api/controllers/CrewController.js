/**
 * CrewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const CrewController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await CrewController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Crew.create(body);
        let updateTypeUser = await User.updateOne({id: body.id_user}).set({type_user: 4});
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id_user} = res.locals.udata.payload;
            let resp = await Crew.find({}).populate('id_user').populate('id_pobus');
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        });
    },

    detail: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const idCrew = res.locals.udata.payload.crew[0].id;
            const resp = await Crew.findOne({id:idCrew}).populate("id_pobus").populate("id_user").meta({skipRecordVerification:true});
            delete resp.id_user.pass_user;
            delete resp.id_user.pin;
            return resp;
        })
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await CrewController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Crew.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Crew.destroyOne({id: req.params.id});
            return true;
        })
    }
}

module.exports = CrewController;

