/**
 * ProtrackController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const crypto = require('crypto-js/md5');
const axios = require('axios');
const TransagenController = require('./TransagenController');
const TranscrewController = require('./TranscrewController');
const KursiController = require('./KursiController');
const BASE_URL_PROTRACK = 'http://api.protrack365.com/api';
const moment = require('moment')
let ACCESS_TOKEN;

const ProtrackController = {
    read : async (req, res) => {
        await LibraryController.response(req, res, async (body) => {
            const {imeis} = req.query;
            if (!imeis) {
                throw new Error('Params imeis must be inquired');
            } 
            const timestamp = Math.floor(Date.now() / 1000).toString();
            const signature = crypto(crypto('Ekamira2021!') + timestamp).toString(); 
            const result = await axios.get(`${BASE_URL_PROTRACK}/authorization?time=${timestamp}&account=eka.master&signature=${signature}`);
            if (result.data.record) {
                ACCESS_TOKEN = result.data.record.access_token;
                const fetchTrack = await axios.get(`${BASE_URL_PROTRACK}/track?access_token=${ACCESS_TOKEN}&imeis=${imeis}`);
                // console.log(fetchTrack.data);
                return fetchTrack.data;
            }
        })
    },

    trace: async (req, res) => {
        LibraryController.response(req, res, async (body) => {
            const imei = await Bus.find({status:0}).meta({skipRecordVerification:true});
            for(i=0;i<imei.length;i++) {
                const timestamp = Math.floor(Date.now() / 1000).toString();
                const signature = crypto(crypto('Ekamira2021!') + timestamp).toString(); 
                const im = imei[i].imei;
                const result = await axios.get(`${BASE_URL_PROTRACK}/authorization?time=${timestamp}&account=eka.master&signature=${signature}`);
                if (result.data.record) {
                    ACCESS_TOKEN = result.data.record.access_token;
                    const fetchTrack = await axios.get(`${BASE_URL_PROTRACK}/track?access_token=${ACCESS_TOKEN}&imeis=${im}`);
                    ProtrackController.updateKursi(fetchTrack.data);
                }
            }
    
            return true;
        })
    },

    updateKursi: async (data) => {
        const lat = data.record[0].latitude;
        const long = data.record[0].longitude;
        const result = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=AIzaSyAONpWmd_3COCC5YkqUxZyjY_IygQrwoSo`);
        const kota = await Kota.find().meta({skipRecordVerification:true});
        const posisi = result.data.plus_code.compound_code;
        let idKota;
        let idKursi = [];
        
        for(i=0;i<kota.length;i++) {
            if(posisi.includes(kota[i].nm_kota)) {
                idKota = kota[i].id;
                break;
            }
        }

        const now = moment().format('YYYY-MM-DD').valueOf();
        const transAgen = await Transagen.find({
            where: {tanggal_smp: {contains:now}}
        }).meta({skipRecordVerification:true});

        for(tg=0;tg<transAgen.length;tg++) {
            const kotatujuan = await Jamtrayek.findOne({id:transAgen[tg].id_jamtrayek}).populate('id_trayek').meta({skipRecordVerification:true});
            if (kotatujuan.id_trayek.id_kota_tujuan == idKota) {
                idKursi.push(transAgen[tg].id_kursi)    
            }
        }
        
        const transCrew = await Transcrew.find({
            where: {tanggal_smp: {contains:now}}
        }).meta({skipRecordVerification:true});

        for(tc=0;tc<transCrew.length;tc++) {
            const kotatujuan = await Jamtrayek.findOne({id:transCrew[tc].id_jamtrayek}).populate('id_trayek').meta({skipRecordVerification:true});
            if (kotatujuan.id_trayek.id_kota_tujuan == idKota) {
                idKursi.push(transCrew[tc].id_kursi)    
            }
        }
        
        const tiket = await Tiket.find({
            where: {tgl_smp: {contains:now}}
        }).populate('id_transcust').meta({skipRecordVerification:true});

        for(tk=0;tk<tiket.length;tk++) {
            const kotatujuan = await Jamtrayek.findOne({id:tiket[tk].id_transcust.id_jamtrayek}).populate('id_trayek').meta({skipRecordVerification:true});
            if (kotatujuan.id_trayek.id_kota_tujuan == idKota) {
                idKursi.push(tiket[tk].id_kursi)    
            }
        }

        // do update kursi
        if(idKursi.length > 0) await Kursi.update({id:idKursi}).set({available:3});
    }

}

module.exports = ProtrackController;

