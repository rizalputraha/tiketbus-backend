/**
 * CheckinbusController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const moment = require('moment');

const CheckinbusController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const dateNow = moment().format('YYYY-MM-DD');

            const idCrew = res.locals.udata.payload.crew[0].id;
            let c = await Checkinbus.findOne({"id_crew":idCrew, "logout":null}).meta({skipRecordVerification:true});
        
            if (c) {
                return [c];
            } else {
                if (req.body.nopol != 0) {
                    const id_bus = await Bus.find({where: {nopol_bus: req.body.nopol}});
                    const checkDateNow = await Checkinbus.find({where: {logout: null, id_bus:id_bus[0].id}});
                    const fill = checkDateNow.filter(e => moment(e.login).format('YYYY-MM-DD') == dateNow);
                    if(fill.length < 2) {
                        const data = {...req.body, id_bus:id_bus[0].id}
                        const resp = id_bus.length == 0 ? "Nopol Tidak Ditemukan" : await CheckinbusController.createProcess(data);
                        return [resp];
                    } else {
                        throw new Error('Kuota Checkin sudah melebihi batas');
                    }
                } else {
                    const checkDateNow = await Checkinbus.find({where: {logout: null, id_bus:req.body.id_bus}});
                    const fill = checkDateNow.filter(e => moment(e.login).format('YYYY-MM-DD') == dateNow);
                    if(fill.length < 2) {
                        const resp = await CheckinbusController.createProcess(req.body);
                        return [resp];
                    } else {
                        throw new Error('Kuota Checkin sudah melebihi batas');
                    }
                }
            }
        });
    },

    createProcess: async function (body) {
        const resp = await Checkinbus.create(body).fetch();
        const updKeteranganBus = await Bus.updateOne({id: body.id_bus}).set({keterangan: 'Timur-Barat'});
        return resp;
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const result = await CheckinbusController.updateProcess(req.params.id, req.body);
            return result;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Checkinbus.updateOne({id: data}).set(reqBody);
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const idCrew = res.locals.udata.payload.crew[0].id;
            
            let resp = await Checkinbus.find({"id_crew": idCrew}).populate('id_bus').meta({skipRecordVerification:true});
            
            return resp;
        });
    },

    check: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const idCrew = res.locals.udata.payload.crew[0].id;
            let resp = await Checkinbus.findOne({"id_crew":idCrew, "logout":null}).populate('id_bus').meta({skipRecordVerification:true});
        
            if (resp) {
                return {"isLogged":true};
            } else {
                return {"isLogged":false};
            }
        })
    }

}

module.exports = CheckinbusController;

