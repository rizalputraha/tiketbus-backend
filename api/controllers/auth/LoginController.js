/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var jwt = require('jsonwebtoken');
// const User = require('../../models/User');
const SESSION = [];
var crypto = require('crypto');
let SALT = 'inibukanapaapa';
const LibraryController = require('../../controllers/LibraryController');
const SignupController = require('./SignupController');

const LoginController = {
    createSalt: function (email, pass) {
        let hash = crypto.createHmac('md5', SALT);
        hash.update(pass);
        hash.update(email.email.toLowerCase());

        let hashed = hash.digest('hex');
        return hashed;
    },

    deleteSession: function (id) {
        const udata = SESSION[`user_${id}`];
        clearTimeout(udata.timeout);
        delete SESSION[`user_${id}`];
        console.log('deleteSession', SESSION);
    },

    sessionExist: function (userdata, crc) {
        const idx = `user_${userdata.id}`;
        const udata = SESSION[idx];

        if (udata) {
            const menit = idleTime / 6000;
            console.log(udata.crc == crc);

            return (udata.crc == crc) ? true : { error: 401, message: `Anda sedang login di device lain, mohon logout!` };
        }
        return { error: 401,
            data: [],
            message: 'Token Invalid! or Session Expired',
            stack: {},
            errorName: '', 
            noExist: true};
    },

    createToken: function (user, type) {
        // type 1 = login mobile, type 0 = website
        let signerOption;
        if (type == 0 || !type) {
            signerOption = {
                issuer: 'mfi',
                expiresIn: '1d',
            }
        } else {
            signerOption = {
                issuer: 'mfi'
            }
        }
        
        let payload = {
            id_user: user.id,
            agen:user.agen,
            pobus:user.pobus,
            crew:user.crew,
            name: user.nm_user,
            alamat: user.addr_user,
            tipe_user: user.type_user,
            email: user.email_user,
            status: user.status,
            telephone: user.telp_user
        }
        const tokenAction = jwt.sign(payload, 'Tku1qGspMSiq3tIebuD2xb96OHAyquABJLfH8xpy1Poo9arztua4qbFyv3o3tIe3JfdeBut8KesogsjGbVx9uspws7OFbBTAiWNlyvf2t7U3Qpgk1I0zJbV0BeNVHrjMkr1DVK62l3rzxiBEdXIiqGy2e8nthb4oJc96HDCyVr0zZnDvlt8T7hEw0N8JTuyf463mr1ocUzhHngpsQYdXI72qwhSdd42xtl77nEBR8zVGGOpHAfFjwlc2fJ7eIdFS', signerOption);
        return tokenAction;
    },

    createTokenMember: function (member) {
        let signerOption = {
            issuer: 'mfi',
            expiresIn: '1d',
        }
        let payload = {
            id_member: member.id,
            name: member.nm_member,
            telpon: member.telp_user,
            alamat: member.addr_member,
            email: member.email_user,
            status: member.status
        }
        const tokenAction = jwt.sign(payload, 'Tku1qGspMSiq3tIebuD2xb96OHAyquABJLfH8xpy1Poo9arztua4qbFyv3o3tIe3JfdeBut8KesogsjGbVx9uspws7OFbBTAiWNlyvf2t7U3Qpgk1I0zJbV0BeNVHrjMkr1DVK62l3rzxiBEdXIiqGy2e8nthb4oJc96HDCyVr0zZnDvlt8T7hEw0N8JTuyf463mr1ocUzhHngpsQYdXI72qwhSdd42xtl77nEBR8zVGGOpHAfFjwlc2fJ7eIdFS', signerOption);
        return tokenAction;
    },

    checkAuthUser: async function (email) {
        let user = await User.findOne({ email_user: email }).populate('agen').populate('crew').populate('pobus');
        return user;
    },

    checkAuthMember: async function (email) {
        let member = await Member.findOne({ email_user: email });
        return member;
    },

    checkPinUser: async function (pin, idUser) {
        const hash = SignupController.createHashPin(pin);
        let user = await User.findOne({ pin: hash, id:idUser }).populate('agen').populate('crew').populate('pobus');
        return user;
    },

    checkPinMember: async function (pin) {
        const hash = SignupController.createHashPin(pin);
        let member = await Member.findOne({ pin: hash });
        return member;
    },

    login: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {type} = req.body
            let checkUser = await LoginController.checkAuthUser(req.body.email);
            if (checkUser) {
                let hashedPw = LoginController.createSalt({ email: checkUser.email_user }, req.body.password);
                if (hashedPw == checkUser.pass_user) {
                    const token = LoginController.createToken(checkUser, type);
                    return token;
                } else {
                    throw new Error('User not Found or Wrong Password!')
                }
            }
            throw new Error('User not Found!');
        })
    },

    loginMember: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let checkMember = await LoginController.checkAuthMember(req.body.email);
            if (checkMember) {
                // let hashedPw = LoginController.createSalt({ email: checkMember.email_user }, req.body.uid);
                if (req.body.uid == checkMember.uid_member) {
                    const token = LoginController.createTokenMember(checkMember);
                    return token;
                } else {
                    throw new Error('Member not Found or Wrong UID!')
                }
            }
            throw new Error('Member not Found!');
        })
    },

    refreshToken: async function (req, res) {
        let signerOption = {
            issuer:'mfi',
            expiresIn: '1d',
        }
        const token = req.body.token;

        const decode = (tok) => {
            return jwt.decode(tok, {complete: true});
        }

        await LibraryController.response(req, res, async (body) => {
            const d = decode(token);
            const idUser = d.payload.id_user;
            let checkUser = await LoginController.checkPinUser(req.body.pin, idUser);
            if (checkUser) {
                const token = LoginController.createToken(checkUser);
                return token;
            }
            throw new Error('Wrong pin!');
        })
    },

    refreshTokenMember: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let checkMember = await LoginController.checkPinMember(req.body.pin);
            if (checkMember) {
                const token = LoginController.createTokenMember(checkMember);
                return token;
            }
            throw new Error('Wrong pin!');
        })
    },


    logout: async function (udata, token) {
        const exists = LoginController.sessionExist(udata, token);
        if (exists === true) {
            const oke = await LoginController.deleteSession(udata.id)
            return true
        }
        return exists;
    }
}

module.exports = LoginController;
