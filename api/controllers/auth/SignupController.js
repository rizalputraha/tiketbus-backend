/**
 * SignupController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var crypto = require('crypto');

const LoginController = require('../auth/LoginController')
const LibraryController = require('../../controllers/LibraryController');

const SignupController = {
  createSalt: function (email, pass) {
        let hash = crypto.createHmac('md5', sails.config.custom.salt);
        hash.update(pass);
        hash.update(email.email.toLowerCase());

        let hashed = hash.digest('hex');
        return hashed;
    },

  register: async function (req, res) {
    await LibraryController.response(req, res, async (body) => {
      let resp = await SignupController.signupProcess(req.body);
      return resp;
    })
  },

  signupProcess: async function (body, tipe) {
    const findEmail = await User.findOne({ email_user: body.email_user });
    const findNotelp = await User.findOne({ telp_user: body.telp_user });
    if (tipe == 2) {
      if (findEmail) {
        throw new Error('Email Sudah Terdaftar');
      } else if (findNotelp) {
        throw new Error('Nomer Telfon Sudah Terdaftar');
      } else {
        let hashed = SignupController.createSalt({ email: body.email_user }, body.pass_user);
        let hashPin = SignupController.createHashPin(body.pin);
        let obj = { ...body, pass_user: hashed, pin: hashPin };
        let res = await User.create(obj).fetch();
        let objEmply = {
          id_pobus: body.id_pobus,
          id_user: res.id
        };
        // console.log({objEmply});
        let addEmply = await Emplypo.create(objEmply)
        return true;
      }
    } else {
      if (findEmail) {
        throw new Error('Email Sudah Terdaftar');
      } else if (findNotelp) {
        throw new Error('Nomer Telfon Sudah Terdaftar');
      } else {
        let hashed = SignupController.createSalt({ email: body.email_user }, body.pass_user);
        let hashPin = SignupController.createHashPin(body.pin);
        let obj = { ...body, pass_user: hashed, pin: hashPin };
        let res = await User.create(obj);
        return true;
      }
    }
  },

  signupMemberProcess: async function (body, tipe) {
    const findEmail = await Member.findOne({ email_user: body.email_user });
    const findNotelp = await Member.findOne({ telp_user: body.telp_user });
    if (findEmail) {
      throw new Error('Email Sudah Terdaftar');
    } else if (findNotelp) {
      throw new Error('Nomer Telfon Sudah Terdaftar');
    } else {
      let hashed = SignupController.createSalt({ email: body.email_user }, body.pass_user);
      let hashPin = SignupController.createHashPin(body.pin);
      let obj = { ...body, pass_user: hashed, pin: hashPin };
      // console.log({obj});
      let res = await Member.create(obj);
      return true;
    }
  },

  createHashPin: function (pin) {
    let hash = crypto.createHmac('sha1', sails.config.custom.salt);
    hash.update(pin);

    let hashed = hash.digest('hex');
    return hashed;
  },
}

module.exports = SignupController;

