/**
 * KursiController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');

const KursiController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await KursiController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Kursi.create(body);
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await Kursi.find({}).meta({skipRecordVerification:true});
            return resp;
        });
    },

    showKursi: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { bus } = req.query;
            const { id_pobus } = res.locals.udata.payload.crew;
            const { id_user } = res.locals.udata.payload;
            if (bus) {
                if (id_pobus != "") {
                    const resp = await Bus.find({id: bus}).populate('kursi').populate('id_pobus').meta({skipRecordVerification:true});
                    const result = resp.filter(e => e.id_pobus.id_pobus == id_pobus);
                    return result;
                } else {
                    const resp = await Bus.find({id: bus}).populate('kursi').populate('id_pobus').meta({skipRecordVerification:true});
                    const result = resp.filter(e => e.id_pobus.id_user == id_user);
                    return result;
                }
            }
            const resp = await Bus.find({}).populate('kursi').populate('id_pobus').meta({skipRecordVerification:true});
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        })
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await KursiController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Kursi.updateOne({id: data}).set(reqBody);
        return true;
    },

    updateCrew: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id } = req.body;
            
            for(i=0; i<id.length;i++) {
                KursiController.updateProcess(id[i], {'available':0})
            }
            return true;
        });
    },

    delete: async function (req,res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Kursi.destroy({id_bus: req.params.id});
            return true;
        })
    }

}

module.exports = KursiController;
