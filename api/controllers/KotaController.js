/**
 * KotaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const axios = require('axios');

const KotaController = {
    kota: async function(req, res) {
        await LibraryController.response(req, res, async (body) => {
    
          let respon = await Kota.find({}).sort('nm_kota ASC');
    
          return respon;
        });
    },

    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await KotaController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        const url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Surabaya,Indonesia&destinations=${body.nm_kota},Indonesia&key=${sails.config.custom.gmaps_key}`;
        const resFetch = await KotaController.fetchApiGmaps(url);
        const jarakMaps = resFetch.data.rows[0].elements[0].distance.value;
        body.jarak = jarakMaps;
        let resp = await Kota.create(body);
        return true;
    },

    fetchApiGmaps: async function (url) {
        try {
            return await axios.get(url);
        } catch (error) {
            return await error;
        }
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await KotaController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Kota.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
      await LibraryController.response(req, res, async (body) => {
          const resp = await Kota.destroyOne({id: req.params.id});
          return true;
      })
    }
}

module.exports = KotaController;

