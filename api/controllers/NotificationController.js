/**
 * NotificationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const https = require('https');
const LibraryController = require('../controllers/LibraryController');

const NotificationController = {
    createNotif : async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const message = { 
                app_id: sails.config.custom.app_id,
                android_channel_id: "adb6b360-e96d-4b8e-830a-bad7954e9653",
                headings: {"en":"Penumpang Baru"},
                contents: {"en": "Ada penumpang baru sedang menunggu"},
                data: {"key":5},
                included_segments: ["All"]
            };
            return await NotificationController.notifProcess(message);
        })
    },

    notifProcess : async (data) => {
        let headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": `Basic ${sails.config.custom.api_key}`
        };

        let opt = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
        };

        const req = https.request(opt, async (res) => {
            res.on('data', async (data) => {
                console.log({response : JSON.parse(data)});
            });
        });

        req.on('error', (e) => {
            console.log({error : e});
        });
        
        req.write(JSON.stringify(data));
        req.end();
        return await 'Success create notification!';
    },

    // checkFirebase: async function (req, res) {
    //     await LibraryController.response(req, res, async (body) => {
    //         const obj = {
    //             email: "checkemail3@gmail.com",
    //             pass: "12345678"
    //         };
    //         const result = await LibraryController.createUserFirebase(obj);
    //         // console.log({result: result});
    //         return result;
    //     })
    // }
};

module.exports = NotificationController;

