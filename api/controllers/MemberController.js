/**
 * MemberController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
const LoginController = require('../controllers/auth/LoginController');
const SignupController = require('../controllers/auth/SignupController');

const MemberController = {
    detailMember: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Member.find({id:id_user});
            return resp;
        })
    },

    listMember: async function (req, res) {
      await LibraryController.response(req, res, async (body) => {
        const {email} = req.query;
        if (email) {
            const resp = await Member.find({select: ['id', 'nm_member', 'addr_member', 'telp_member', 'email_user'], where: {status: 0, email_user:email}});
            return resp;
        }
        const resp = await Member.find({select: ['id', 'nm_member', 'addr_member', 'telp_member', 'email_user'], where: {status: 0}});
        return resp;
      });
    },
    
    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const proc = await MemberController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const result = await LibraryController.createUserFirebase(req.body);
            // console.log({result});
            if (result.error == 500) {
                throw new Error(result);
            } else {
                req.body.uid_member = result;
                // console.log({ch: req.body});
                let resp = await SignupController.signupMemberProcess(req.body);
                return result;
            }
        })
    },

    updateProcess: async function (data, reqBody) {
        let hashed = "";
        let hashPin = "";
        let obj;
        if (reqBody.pass_user != undefined){
            hashed = LoginController.createSalt({email: reqBody.email_user}, reqBody.pass_user);
            obj = {...reqBody, pass_user: hashed};
        }

        if (reqBody.pin != undefined) {
            hashPin = SignupController.createHashPin(reqBody.pin);
            obj = {...reqBody, pin: hashPin};
        }

        if (reqBody.pass_user != undefined && reqBody.pin != undefined) {
            obj = {...reqBody, pass_user: hashed, pin: hashPin};
        } else {
            obj = {...reqBody};
        }
        
        // const resFirebase = await LibraryController.updateUserFirebase(data, obj);
        // if (resFirebase.error == 500) {
        //     throw new Error(resFirebase);
        // } else {
            const result = await Member.updateOne({id: data}).set(obj);
            return true;
        // }
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resFirebase = await LibraryController.deleteUserFirebase(req.params.uid);
            if (resFirebase.error == 500) {
                throw new Error(resFirebase);
            } else {
                const resp = await Member.destroyOne({uid_member: req.params.uid});
                return resFirebase;
            }
        })
    }
}

module.exports = MemberController;

