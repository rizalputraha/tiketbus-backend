/**
 * BusController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const LibraryController = require('../controllers/LibraryController');
// var jwt = require('jsonwebtoken');

const BusController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await BusController.createProcess(req.body);
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Bus.create(body).fetch();
        const objKursi = LibraryController.sliceNumberString(resp.id);
        const respKursi = await Kursi.createEach(objKursi);
        return true;
    },

    listBus: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const {id_user} = res.locals.udata.payload;
            let resp = await Bus.find({}).populate('id_pobus').populate('id_trayek').meta({skipRecordVerification:true});
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        });
    },

    detail: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id } = req.params;
            let resp = await Bus.findOne({id:id}).meta({skipRecordVerification:true});
            return resp;
        })
    },

    update: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            if(req.body.keterangan == "Berangkat") {
                req.body.keterangan = "Timur-Barat"
            } else if(req.body.keterangan = "Pulang") {
                req.body.keterangan = "Barat-Timur"
            }
            const proc = await BusController.updateProcess(req.params.id, req.body);
            return proc;
        });
    },

    updateProcess: async function (data, reqBody) {
        const result = await Bus.updateOne({id: data}).set(reqBody);
        return true;
    },

    delete: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Bus.destroyOne({id: req.params.id});
            return true;
        })
    }

}

module.exports = BusController;

