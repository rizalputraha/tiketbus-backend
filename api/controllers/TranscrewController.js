/**
 * TranscrewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const LibraryController = require('../controllers/LibraryController');
const KursiController = require('../controllers/KursiController');
const moment = require('moment');

const TranscrewController = {
    create: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id_pobus, id_crew, id_jamtrayek, id_kursi, tanggal, total_hrg, status, id_bus } = req.body;
            let tanggal_smp;
            const jt = await Jamtrayek.findOne({id:id_jamtrayek}).meta({skipRecordVerification:true});
            const start = moment(jt.jam_berangkat, "HH:mm");
            const end = moment(jt.jam_sampai, "HH:mm");
            if(end.diff(start) < 0 ) {
                tanggal_smp = moment().add(1, 'd').format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
            } else {
                tanggal_smp = moment().format("YYYY-MM-DD") + " " + end.format('HH:mm:ss')
            }

            const data = {
                kd_transaksi: 'CRW' + moment().format('DDMMYY') + id_crew + LibraryController.randomNumber(),
                id_crew,
                id_pobus,
                id_jamtrayek,
                id_kursi,
                tanggal,
                tanggal_smp,
                id_bus,
                status,
                total_hrg,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss').valueOf(),
            }
            let resp = await TranscrewController.createProcess(data);
            let kursi = await KursiController.updateProcess(req.body.id_kursi, { available: 1 });
            return resp;
        })
    },

    createProcess: async function (body) {
        let resp = await Transcrew.create(body);
        return true;
    },

    read: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { id_user } = res.locals.udata.payload;
            const id_crew = res.locals.udata.payload.crew[0].id;

            if (id_crew != "") {
                let resp = await Transcrew.find({ 'id_crew': id_crew }).populate('id_crew').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate("id_bus").meta({skipRecordVerification:true});
                for (let i = 0; i < resp.length; i++) {
                    let el = resp[i];
                    const t = await Trayek.findOne({ id: el.id_jamtrayek.id_trayek }).populate('id_kota_asal').populate('id_kota_tujuan').meta({skipRecordVerification:true});
                    el.id_jamtrayek.id_trayek = t;
                }
                return resp;
            }

            let resp = await Transcrew.find({}).populate('id_crew').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate("id_bus").meta({skipRecordVerification:true});
            const result = resp.filter(e => e.id_pobus.id_user == id_user);
            return result;
        });
    },

    historyPo: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            let resp = await Transcrew.find({}).populate('id_crew').populate('id_pobus').populate('id_jamtrayek').populate('id_kursi').populate('id_bus').meta({skipRecordVerification:true});
            for (let i = 0; i < resp.length; i++) {
                let el = resp[i];
                el.id_crew.id_user = await User.findOne({id: el.id_crew.id_user}).meta({skipRecordVerification:true});
                el.id_jamtrayek.id_trayek = await Trayek.findOne({ id: el.id_jamtrayek.id_trayek }).meta({skipRecordVerification:true});
                const resKotaAsal = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_asal }).meta({skipRecordVerification:true});
                const resKotaTujuan = await Kota.findOne({ id: el.id_jamtrayek.id_trayek.id_kota_tujuan }).meta({skipRecordVerification:true});
                el.id_jamtrayek.id_trayek.k_asal = resKotaAsal.nm_kota;
                el.id_jamtrayek.id_trayek.k_tujuan = resKotaTujuan.nm_kota;
            }
            return resp;
        });
    },

    historyTransaksi: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const resp = await Transcrew.find({}).populate('id_crew').populate('id_pobus').populate('id_trayek').populate('id_kursi').meta({skipRecordVerification:true});
            for (let i = 0; i < resp.length; i++) {
                let el = resp[i];
                el.id_jamtrayek.id_trayek = await Trayek.findOne({ id: el.id_jamtrayek.id_trayek }).meta({skipRecordVerification:true});
            }
            return resp;
        })
    },

    detailKursi: async function (req, res) {
        await LibraryController.response(req, res, async (body) => {
            const { kdKursi } = req.params;
            const crew = await Transcrew.find({ where: { id_kursi: kdKursi } }).sort("created_at DESC").populate('id_jamtrayek').meta({skipRecordVerification:true});
            const agen = await Transagen.find({ where: { id_kursi: kdKursi } }).sort("created_at DESC").populate('id_jamtrayek').meta({skipRecordVerification:true});
            const cust = await Tiket.find({ where: { id_kursi: kdKursi } }).sort("created_at DESC").populate('id_transcust').meta({skipRecordVerification:true});
            
            let arr = [];
            let d;
            if(crew.length > 0) {
                d = new Date(moment(crew[0].created_at).format('YYYY-MM-DD HH:mm:ss').valueOf());
                crew[0].date = d.getTime();
                arr.push(crew[0]);
            }
            
            if(agen.length > 0) {
                d = new Date(moment(agen[0].created_at).format('YYYY-MM-DD HH:mm:ss').valueOf());
                agen[0].date = d.getTime();
                arr.push(agen[0]);
            }
            
            if(cust.length > 0) {
                cust[0].id_jamtrayek = cust[0].id_transcust.id_jamtrayek;
                delete cust[0].id_transcust;
                d = new Date(moment(cust[0].created_at).format('YYYY-MM-DD HH:mm:ss').valueOf());
                cust[0].date = d.getTime();
                arr.push(cust[0]);
            }
            const fin = arr.sort((a,b) => b.date - a.date);
            console.log(fin);
            const t = await Jamtrayek.findOne({where: {id: fin[0].id_jamtrayek.id}}).populate("id_trayek").meta({skipRecordVerification:true});
            fin[0].id_jamtrayek = t;
            const kota = await Trayek.findOne({where: {id: t.id_trayek.id}}).populate("id_kota_asal").populate("id_kota_tujuan").meta({skipRecordVerification:true});
            fin[0].id_jamtrayek.id_trayek = kota;
            return fin[0];
        })
    },

}

module.exports = TranscrewController;

