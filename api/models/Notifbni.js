/**
 * Notifbni.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_notif_bni",
  attributes: {
    id: {
      type: 'number',
      columnName: 'idnotif_bni',
      autoIncrement: true
    },

    trx_id: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    virtual_account: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    customer_name: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    trx_amount: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    payment_amount: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    cumulative_payment_amount: {
      type: 'string',
      // required: true,
      maxLength: 100
    },

    payment_ntb: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    datetime_payment: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    datetime_payment_iso8601: {
      type: 'string',
      required: true,
      maxLength: 100
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};



