/**
 * Bus.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_bus",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_bus',
      autoIncrement: true
    },
    
    id_pobus: {
      model: 'pobus'
    },

    kd_bus: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    nopol_bus: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    lastloc_bus: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    keterangan: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    status: {
      type: 'number'
    },

    id_trayek: {
      model:'trayek'
    },

    kursi: {
      collection: 'kursi',
      via: 'id_bus'
    },

    jam_trayek: {
      collection: 'jamtrayek',
      via:'id_bus'
    },

    imei: {
      type: 'string'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

