/**
 * Tiket.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_tiket",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_tiket',
      autoIncrement: true
    },
    
    id_kursi: {
      model: 'kursi'
    },

    id_transcust: {
      model: 'transcust'
    },

    nama: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    no_telp: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    harga: {
      type: 'number',
      required: true
    },

    status: {
      type: 'number',
      required: true,
    },

    tgl_beli: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    tgl_brgkt: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    tgl_smp: {
      type: 'string',
    },

    created_at: {
      type: 'string',
      maxLength: 100
    },

    updated_at: {
      type: 'string',
      maxLength: 100
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

