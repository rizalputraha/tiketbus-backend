/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_user",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_user',
      autoIncrement: true
    },
    
    nm_user: {
      type: 'string',
      required: true,
      maxLength: 45
    },

    addr_user: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    telp_user: {
      type: 'string',
      required: true,
      maxLength: 20,
      unique: true
    },

    type_user: {
      type: 'number',
      required: true,
    },

    email_user: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 100
    },

    pass_user: {
      type: 'string',
      required: true,
      protect: true
    },

    status: {
      type: 'number',
    },

    player_id: {
      type: 'string',
      maxLength: 100
    },

    pin: {
      type: 'string',
    },


    agen: {
      collection:'agen',
      via:'id_user'
    },
    crew: {
      collection: 'crew',
      via:'id_user'
    },
    pobus: {
      collection: 'pobus',
      via:'id_user'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },
};

