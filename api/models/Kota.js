/**
 * Kota.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 't_kota',

  attributes: {
    id: {
      type: 'number',
      columnName: 'id_kota',
      autoIncrement: true
      // required: true
    },
    nm_kota : {
      type: 'string',
      required: true,
    },
    jarak : {
      type: 'string'
    },
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

  trayeks: {
    collection: 'trayek',
    via: 'trayek'
  },

};

