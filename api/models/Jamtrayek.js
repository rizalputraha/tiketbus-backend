/**
 * Jamtrayek.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_jamtrayek",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_jamtrayek',
      autoIncrement: true
    },

    id_trayek: {
      model: 'trayek'
    },

    id_bus: {
      model: 'bus'
    },

    jam_berangkat: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    jam_sampai: {
      type: 'string',
      required: true,
      maxLength: 50
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

