/**
 * Memberva.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_memberva",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_memberva',
      autoIncrement: true
    },

    id_member: {
      model:'member'
    },

    id_bank: {
      model:'bank'
    },

    status: {
      type: 'number'
    },

    kd_va: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    created_at: {
      type: 'string',
      maxLength: 100
    },

    updated_at: {
      type: 'string',
      maxLength: 100
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

