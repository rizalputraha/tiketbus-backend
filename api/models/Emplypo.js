/**
 * Emplypo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_emplypo",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_emplypo',
      autoIncrement: true
    },
    
    id_pobus: {
      model: 'pobus'
    },

    id_user: {
      model: 'user'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

