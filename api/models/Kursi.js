/**
 * Kursi.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_kursi",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_kursi',
      autoIncrement: true
    },

    id_bus: {
      model: 'bus'
    },

    kd_kursi: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    available: {
      type: 'string',
      required: true,
      maxLength: 50
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

