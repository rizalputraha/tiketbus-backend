/**
 * Transcust.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_transcust",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_transcust',
      autoIncrement: true
    },

    kd_transaksi: {
      type: 'string',
      required: true,
      maxLength: 100
    },
    
    id_pobus: {
      model: 'pobus'
    },

    id_bus: {
      model: 'bus'
    },

    id_jamtrayek: {
      model: 'jamtrayek'
    },

    id_member: {
      model: 'member'
    },

    id_memberva: {
      model: 'memberva'
    },

    qty: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    total: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    status: {
      type: 'number',
      required: true
    },

    due_date: {
      type: 'string',
      maxLength: 50
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

