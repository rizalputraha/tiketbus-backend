/**
 * Transcrew.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_transcrew",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_transcrew',
      autoIncrement: true
    },

    kd_transaksi: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    id_crew: {
      model: 'crew'
    },

    id_pobus: {
      model: 'pobus'
    },

    id_bus: {
      model: 'bus'
    },

    id_jamtrayek: {
      model: 'jamtrayek'
    },

    id_kursi: {
      model: 'kursi'
    },

    tanggal: {
      type: 'string',
      required: true,
      maxLength: 50
    },
    
    tanggal_smp: {
      type: 'string',
    },

    status: {
      type: 'number',
      required: true
    },

    total_hrg: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

