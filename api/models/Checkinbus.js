/**
 * Checkinbus.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_checkinbus",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_checkinbus',
      autoIncrement: true
    },

    id_bus: {
      model:'bus'
    },

    id_crew: {
      model:'crew'
    },

    login: {
      type: 'string',
      required: false,
      maxLength: 50
    },

    logout: {
      type: 'string',
      allowNull: true,
      maxLength: 50
    },

    status: {
      type: 'number', 
      defaultsTo: 0
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

