/**
 * Crew.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_crew",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_crew',
      autoIncrement: true
    },

    id_pobus: {
      model: 'pobus'
    },

    id_user: {
      model: 'user'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

