/**
 * Member.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_member",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_member',
      autoIncrement: true
    },

    uid_member: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    nm_member: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    addr_member: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    telp_user: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    email_user: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    status: {
      type: 'number'
    },

    player_id: {
      type: 'string',
      maxLength: 100
    },

    pin: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    pass_user: {
      type: 'string',
      required: true,
      maxLength: 100
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

