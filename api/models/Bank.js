/**
 * Bank.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_bank",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_bank',
      autoIncrement: true
    },

    nm_bank: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    status: {
      type: 'number'
    },

    created_at: {
      type: 'string',
      maxLength: 100
    },

    updated_at: {
      type: 'string',
      maxLength: 100
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

