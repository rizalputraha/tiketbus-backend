/**
 * Pobus.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_pobus",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_pobus',
      autoIncrement: true
    },

    nm_pobus: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    addr_pobus: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    id_user: {
      model: 'user'
    },

    pobus: {
      collection: 'bus',
      via: 'id_pobus'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

