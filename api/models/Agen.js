/**
 * Agen.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_agen",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_agen',
      autoIncrement: true
    },

    nm_agen: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    addr_agen: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    max_order: {
      type: 'number',
      required: true,
    },

    id_pobus: {
      model: 'pobus'
    },

    id_user: {
      model: 'user'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};

