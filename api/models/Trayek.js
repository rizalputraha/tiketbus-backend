/**
 * Trayek.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_trayek",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_trayek',
      autoIncrement: true
    },

    nama: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    // k_asal: {
    //   type: 'string',
    //   required: true,
    //   maxLength: 100
    // },

    // k_tujuan: {
    //   type: 'string',
    //   required: true,
    //   maxLength: 50
    // },

    id_kota_asal: {
      model:'kota'
    },

    id_kota_tujuan: {
      model:'kota'
    },

    harga: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    keterangan: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    id_pobus: {
      model:'pobus'
    },


    jam_trayek: {
      collection: 'jamtrayek',
      via: 'id_trayek'
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    },

    start_tgl_event: {
      type:'string',
      allowNull: true
    },

    end_tgl_event: {
      type:'string',
      allowNull: true
    }

    // list_bus: {
    //   collection: 'bus',
    //   via: 'id_trayek'
    // }
  },

};

