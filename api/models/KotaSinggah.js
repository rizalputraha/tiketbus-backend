/**
 * KotaSinggah.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

 module.exports = {

  tableName: "t_kota_singgah",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_kota_singgah',
      autoIncrement: true
    },

    id_trayek: {
      model:'trayek'
    },

    id_kota: {
      model:'kota'
    },

    status: {
      type: 'number'
    },

    deletedAt: {
      type: 'string',
      allowNull: true
    }
  },

};
