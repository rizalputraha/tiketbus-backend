/**
 * Paymentcust.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_paymentcust",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_paymentcust',
      autoIncrement: true
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

