/**
 * Transagen.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_transagen",
  attributes: {
    id: {
      type: 'number',
      columnName: 'id_transagen',
      autoIncrement: true
    },

    kd_transaksi: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    id_pobus: {
      model: 'pobus'
    },

    id_agen: {
      model: 'agen'
    },

    id_jamtrayek: {
      model: 'jamtrayek'
    },

    id_kursi: {
      model: 'kursi'
    },

    id_bus: {
      model: 'bus'
    },

    tanggal: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    tanggal_smp: {
      type: 'string',
    },

    nama: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    qty: {
      type: 'string',
      required: true,
      maxLength: 50
    },

    total_hrg: {
      type: 'string',
      required: true,
      maxLength: 100
    },

    no_telp: {
      type: 'string',
      required: true,
    },

    status: {
      type: 'number',
    },

    created_at: {
      type: 'string',
      autoCreatedAt: true,
    },
    updated_at: {
      type: 'string',
      autoUpdatedAt: true,
    },
      
    deletedAt: {
      type: 'string',
      allowNull: true
    }

  },

};

