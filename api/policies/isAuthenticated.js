var jwt = require('jsonwebtoken');

module.exports=function(req, res, next) {
    let signerOption = {
        issuer:'mfi',
        expiresIn: '1d',
    };
    let signerOption1 = {
        issuer:'mfi'
    };
    const token = req.headers['npc-token'] || false;
    const verify = (tok) => {
        try {
            return jwt.verify(tok, 'Tku1qGspMSiq3tIebuD2xb96OHAyquABJLfH8xpy1Poo9arztua4qbFyv3o3tIe3JfdeBut8KesogsjGbVx9uspws7OFbBTAiWNlyvf2t7U3Qpgk1I0zJbV0BeNVHrjMkr1DVK62l3rzxiBEdXIiqGy2e8nthb4oJc96HDCyVr0zZnDvlt8T7hEw0N8JTuyf463mr1ocUzhHngpsQYdXI72qwhSdd42xtl77nEBR8zVGGOpHAfFjwlc2fJ7eIdFS', signerOption)
        } catch (error) {
            try {
                return jwt.verify(tok, 'Tku1qGspMSiq3tIebuD2xb96OHAyquABJLfH8xpy1Poo9arztua4qbFyv3o3tIe3JfdeBut8KesogsjGbVx9uspws7OFbBTAiWNlyvf2t7U3Qpgk1I0zJbV0BeNVHrjMkr1DVK62l3rzxiBEdXIiqGy2e8nthb4oJc96HDCyVr0zZnDvlt8T7hEw0N8JTuyf463mr1ocUzhHngpsQYdXI72qwhSdd42xtl77nEBR8zVGGOpHAfFjwlc2fJ7eIdFS', signerOption1)
            } catch (e) {
                console.log('VERIFY ERR ', error.message, error.name);
                res.status(200);
                return res.json({error: 401,
                    data: [],
                    message: `${error.message} or Token Invalid!`,
                    stack: {},
                    errorName: ''});
            }
        }
    }
    const decode = (tok) => {
        return jwt.decode(tok, {complete: true});
    }

    if (token && verify(token)) {
        res.locals.udata = decode(token);
        // console.log(res.locals, {token});
        next();
    } else {
        res.status(200)
        return res.json({error: 401,
            data: [],
            message: 'Token Invalid! or Session Expired',
            stack: {},
            errorName: ''});
    }

    
}